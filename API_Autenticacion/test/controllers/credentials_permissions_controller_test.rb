require 'test_helper'

class CredentialsPermissionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @credentials_permission = credentials_permissions(:one)
  end

  test "should get index" do
    get credentials_permissions_url, as: :json
    assert_response :success
  end

  test "should create credentials_permission" do
    assert_difference('CredentialsPermission.count') do
      post credentials_permissions_url, params: { credentials_permission: { credential_id: @credentials_permission.credential_id, permission_id: @credentials_permission.permission_id } }, as: :json
    end

    assert_response 201
  end

  test "should show credentials_permission" do
    get credentials_permission_url(@credentials_permission), as: :json
    assert_response :success
  end

  test "should update credentials_permission" do
    patch credentials_permission_url(@credentials_permission), params: { credentials_permission: { credential_id: @credentials_permission.credential_id, permission_id: @credentials_permission.permission_id } }, as: :json
    assert_response 200
  end

  test "should destroy credentials_permission" do
    assert_difference('CredentialsPermission.count', -1) do
      delete credentials_permission_url(@credentials_permission), as: :json
    end

    assert_response 204
  end
end
