require 'test_helper'

class CredentialsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @credential = credentials(:one)
  end

  test "should get index" do
    get credentials_url, as: :json
    assert_response :success
  end

  test "should create credential" do
    assert_difference('Credential.count') do
      post credentials_url, params: { credential: { email: @credential.email, password: @credential.password, person_id: @credential.person_id } }, as: :json
    end

    assert_response 201
  end

  test "should show credential" do
    get credential_url(@credential), as: :json
    assert_response :success
  end

  test "should update credential" do
    patch credential_url(@credential), params: { credential: { email: @credential.email, password: @credential.password, person_id: @credential.person_id } }, as: :json
    assert_response 200
  end

  test "should destroy credential" do
    assert_difference('Credential.count', -1) do
      delete credential_url(@credential), as: :json
    end

    assert_response 204
  end
end
