# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170523043558) do

  create_table "certificates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "credential_id"
    t.datetime "date_certificate"
    t.string   "token"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["credential_id"], name: "index_certificates_on_credential_id", using: :btree
  end

  create_table "credentials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "person_id"
    t.string   "email",              limit: 50
    t.string   "password_digest",                               null: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "confirmation_token"
    t.boolean  "activated",                     default: false
    t.index ["person_id"], name: "index_credentials_on_person_id", using: :btree
  end

  create_table "credentials_permissions", primary_key: ["credential_id", "permission_id"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "credential_id", null: false
    t.integer "permission_id", null: false
    t.index ["credential_id"], name: "index_credentials_permissions_on_credential_id", using: :btree
    t.index ["permission_id"], name: "index_credentials_permissions_on_permission_id", using: :btree
  end

  create_table "people", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "last_name",    limit: 50
    t.string   "first_name",   limit: 50
    t.string   "dui",          limit: 13
    t.string   "municipality", limit: 40
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "permissions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "permission_name", limit: 50
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_foreign_key "certificates", "credentials"
  add_foreign_key "credentials", "people"
  add_foreign_key "credentials_permissions", "credentials"
  add_foreign_key "credentials_permissions", "permissions"
end
