class AddConfirmationToken < ActiveRecord::Migration[5.0]
  def change
    add_column :credentials, :confirmation_token, :string
    add_column :credentials, :activated, :boolean, default: false
  end
end
