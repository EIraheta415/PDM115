class PasswordCheck

  prepend SimpleCommand


  def initialize(token, secret)
    @token = token
    @secret = secret
  end

  def call
    info
  end

  private

  attr_accessor :token, :secret

  def info
    @info ||= JsonWebToken.decode(token, secret)
    @info || errors.add(:token, 'Invalid password token') && nil
  end

end
