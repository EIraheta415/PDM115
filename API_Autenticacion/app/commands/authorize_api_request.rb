class AuthorizeApiRequest
  prepend SimpleCommand

#  def initialize(headers = {})
#    @headers = headers
#  end

  def initialize(token)
    @token = token
  end


  def call
    user
  end

  private

  #attr_reader :headers
  attr_reader :token

  def user
    @credential ||= Credential.find_by(email: decoded_auth_token[:user_email]) if decoded_auth_token
    @credential || errors.add(:token, 'Invalid token') && nil
  end

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(token, Rails.application.secrets.secret_key_base)
  end

=begin
  def http_auth_header
    if headers['Authorization'].present?
      return headers['Authorization'].split(' ').last
    else
      errors.add(:token, 'Missing token')
    end
    nil
  end
=end


end
