class VerificationGen
  prepend SimpleCommand


  def initialize(email, date)
    @email = email
    @date = date
  end

  def call
    JsonWebToken.encode({user_email: email, date_verified: date}, Rails.application.secrets.secret_key_base)
  end

  private

  attr_accessor :email, :date




end
