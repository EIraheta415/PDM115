class CertificateCheck
  prepend SimpleCommand

  #@params: email of the user who completed the course, organization that grants the certificate, name of the course
  #@returN: Json web token that contains the parameters encoded with a server secret

  def initialize(token)
    @token = token
  end

  def call
    info
  end

  private

  attr_accessor :token

  def info
    @info ||= JsonWebToken.decode(token, Rails.application.secrets.certificate_key)
    @info || errors.add(:token, 'Invalid certitificate') && nil
  end


end
