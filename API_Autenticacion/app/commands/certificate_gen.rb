class CertificateGen
  prepend SimpleCommand

  #@params: email of the user who completed the course, organization that grants the certificate, name of the course
  #@returN: Json web token that contains the parameters encoded with a server secret

  def initialize(email, organization, course)
    @email = email
    @organization = organization
    @course = course
  end

  def call
    JsonWebToken.encode({user_email: email, organization: organization, course: course}, Rails.application.secrets.certificate_key)
  end

  private

  attr_accessor :email, :organization, :course




end
