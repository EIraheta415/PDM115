class ConfirmationCheck

  prepend SimpleCommand


  def initialize(token, secret)
    @token = token
    @secret = secret
  end

  def call
    info
  end

  private

  attr_accessor :token, :secret

  def info
    @info ||= JsonWebToken.decode(token, secret)
    @info || errors.add(:token, 'Invalid token') && nil
  end

end
