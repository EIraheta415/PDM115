class AuthenticateUser
  prepend SimpleCommand

  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    JsonWebToken.encode({user_id: credential.id, user_email: credential.email}, Rails.application.secrets.secret_key_base, 24) if credential
  end

  private

  attr_accessor :email, :password

  def credential
    credential = Credential.find_by_email(email)

    return credential if credential && credential.authenticate(password) && is_verified(credential) #Default provided by bcyrpt

    errors.add :credentials, 'Invalid credentials or account not activated'
    nil
  end

  def is_verified(credential)
    @decoded_token ||= JsonWebToken.decode(credential.confirmation_token, Rails.application.secrets.secret_key_base)
    @decoded_token || nil
  end

end
