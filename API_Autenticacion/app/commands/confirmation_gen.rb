class ConfirmationGen
  prepend SimpleCommand

  #@params: email of the new user, date of account creation,
  #activated describes if the account has been activated, always false since we're generating the confirmation token
  #@returN: Json web token that contains the token to be mailed

  def initialize(email, user_secret)
    @email = email
    @activated = false
    @user_secret = user_secret
  end

  def call
    JsonWebToken.encode({user_email: email, activated: activated}, user_secret)
  end

  private

  attr_accessor :email, :user_secret, :activated




end
