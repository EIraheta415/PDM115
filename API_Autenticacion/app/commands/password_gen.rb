class PasswordGen
  prepend SimpleCommand

  def initialize(email, secret, date)
    @email = email
    @secret = secret
    @date = date
  end

  def call
    JsonWebToken.encode({user_email: email, date_requested: date}, secret)
  end

  private

  attr_accessor :email, :secret, :date




end
