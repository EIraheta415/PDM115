class CertificatesController < ApplicationController
  before_action :set_certificate, only: [:show, :update, :destroy]
  #before_action :verify_course_admin, only: [:create, :destroy, :update]

  # GET /certificates
  def index
    @certificates = Certificate.all

    render json: @certificates
  end

  # GET /certificates/1
  def show
    render json: @certificate
  end

  # POST /certificates
  def create

    credential = Credential.find_by(email: params[:email])
    currentcertificate = Certificate.find_by(credential_id: credential.id) if credential

    if credential && !currentcertificate
     #puts user.name
     command = CertificateGen.call(params[:email], params[:organization], params[:course])


     if command.success?
       @token = command.result
       @certificate = Certificate.new({credential_id: credential.id, token: @token})

       if @certificate.save
         render json: @certificate, status: :created, location: @certificate
       else
         render json: @certificate.errors, status: :unprocessable_entity
       end

     else
       render json: {error: 'Could not grant certificate'}
     end

   else
      render json: {error: 'User does not exist or already has a certificate'}
   end
=begin
    @certificate = Certificate.new(certificate_params)

    if @certificate.save
      render json: @certificate, status: :created, location: @certificate
    else
      render json: @certificate.errors, status: :unprocessable_entity
    end
=end
end

  def check

    command = CertificateCheck.call(params[:certificate])

    if command.success?
      puts 'success'
      render json: command.result
    else
      render json: {error: 'Not a valid certificate'}
  end

  end



  # PATCH/PUT /certificates/1
  def update
    if @certificate.update(certificate_params)
      render json: @certificate
    else
      render json: @certificate.errors, status: :unprocessable_entity
    end
  end

  # DELETE /certificates/1
  def destroy
    @certificate.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_certificate
      @certificate = Certificate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def certificate_params
      params.require(:certificate).permit(:credential_id, :date_certificate, :token)
    end
end
