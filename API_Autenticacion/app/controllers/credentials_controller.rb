class CredentialsController < ApplicationController
  before_action :set_credential, only: [:show, :update, :destroy, :show_permissions]
  #before_action :extract_token, only: [:update, :destroy, :password_reset]
  # GET /credentials
  def index
    @credentials = Credential.all

    render json: @credentials
  end

  # GET /credentials/1
  def show
    render json: @credential
  end

  # POST /credentials
  def create
    @permission_name = params[:permission]
    params.delete :permission
    @credential = Credential.new(credential_params)
    if @credential.save
      #Assign the permission_name
      @credential.permissions << Permission.find_by(permission_name: @permission_name)

      command = ConfirmationGen.call(@credential.email, @credential.confirmation_token)
      if command.success?
        # Mail the token with the Usermailer, its in command.result
        UserMailer.send_signup_email(@credential.email, command.result).deliver
        render json: @credential, status: :created, location: @credential
      else
        render json: @credential.errors, status: :unprocessable_entity
      end

    else
      render json: @credential.errors, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /credentials/1
  def update

    #if current_user.id.to_s == params[:id].to_s
      credential_params.delete :password
      if @credential.update(credential_params)
        render json: @credential
      else
        render json: @credential.errors, status: :unprocessable_entity
      end
    #else
     # render json: {error: 'Not authorized'}, status: 403
    #end
  end

  # DELETE /credentials/1
  def destroy
    #if (current_user.id.to_s == params[:id].to_s)
      @credential.destroy
      render json: {message: 'deleted'}, status: 200
    #else
    #  render json: {error: 'Not authorized'}
    #end
  end

  def show_permissions
    render json: @credential.permissions
  end

  def verify_email
    @credential_to_verify = Credential.find_by(email: params[:email])
    @secret = @credential_to_verify.confirmation_token
    puts 'The secret is: '
    puts @secret

    if @credential_to_verify
      decode_with_db = ConfirmationCheck.call(params[:token], @secret)

      if decode_with_db.success?
        command3 = VerificationGen.call(@credential_to_verify.email, Time.now)
        @credential_to_verify.update({confirmation_token: command3.result})
        render json: {email: @credential_to_verify.email, activated_at: Time.now}

      else
        render json: {error: decode_with_db.errors}
      end
    else
      render json: {error: 'Token already used, your account should be active'}
    end


  end

  def password_reset
    @credential_to_reset = Credential.find(params[:user_id])
    # The password hash is the secret the jwt is encrypted with
    generate_token = PasswordGen.call(@credential_to_reset.email, @credential_to_reset.password_digest, Time.now)

    if generate_token.success?
      #Send email with generated token
      UserMailer.send_password_reset(@credential_to_reset.email, @credential_to_reset.id, generate_token.result)
      render json: {email: @credential_to_reset.email, state: 'Email sent', token: generate_token.result}
    else
      render json: {error: generate_token.errors}
    end
  end

  def password_change
    @credential_to_change = Credential.find(params[:user_id])

    verify_pass_token = PasswordCheck.call(params[:token], @credential_to_change.password_digest)
    if verify_pass_token.success?
      @credential_to_change.update({password: params[:new_password], password_confirmation: params[:new_password_confirmation]})
      render json: {success: 'Password changed'}
    else
      render json: {error: 'This token is invalid'}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credential
      @credential = Credential.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def credential_params
      #params.require(:credential).permit(:person_id, :email, :password, :password_confirmation)
      #params.require(:credential).permit(:email, :password, :password_confirmation, :first_name, :last_name, :dui, :municipality)
      params.permit(:email, :password, :password_confirmation, :first_name, :last_name, :dui, :municipality)
    end

    wrap_parameters :credential, include: [:email, :password, :password_confirmation, :first_name, :last_name, :dui, :municipality]
end
