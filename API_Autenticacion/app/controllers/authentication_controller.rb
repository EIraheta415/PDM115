class AuthenticationController < ApplicationController
  #skip_before_action :authenticate_request

  def authenticate

    command = AuthenticateUser.call(params[:email], params[:password])

    if command.success?
      render json: {auth_token: command.result}
    else
      render json: {error: command.errors}, status: :unauthorized

    end

  end

  def check

    command = AuthorizeApiRequest.call(params[:token])

    if command.success?
      render json: {id: command.result.id, email: command.result.email, first_name: command.result.first_name, last_name: command.result.last_name, dui: command.result.dui,
  municipality: command.result.municipality, permissions: command.result.permissions.select('permission_name').map(&:permission_name)}
      
    else
      render json: {error: 'Not a valid token'}
    end

  end

end
