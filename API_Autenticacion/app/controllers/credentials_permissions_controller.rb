class CredentialsPermissionsController < ApplicationController
  before_action :set_credentials_permission, only: [:show, :update, :destroy]
  #before_action :verify_cred_admin, only: [:create, :update, :destroy]

  # GET /credentials_permissions
  def index
    @credentials_permissions = CredentialsPermission.all

    render json: @credentials_permissions
  end

  # GET /credentials_permissions/1
  def show
    email = @credentials_permission.credential[:email]
    perm_name = @credentials_permission.permission[:permission_name]
    render json: {user: email, permission: perm_name}#@credentials_permission
  end

  # POST /credentials_permissions
  def create
    @credentials_permission = CredentialsPermission.new(credentials_permission_params)

    if @credentials_permission.save
      render json: @credentials_permission, status: :created#, location: @credentials_permission
    else
      render json: @credentials_permission.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /credentials_permissions/1
  def update
    if @credentials_permission.update(credentials_permission_params)
      render json: @credentials_permission
    else
      render json: @credentials_permission.errors, status: :unprocessable_entity
    end
  end

  # DELETE /credentials_permissions/1
  def destroy
    @credentials_permission.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credentials_permission
      @credentials_permission = CredentialsPermission.find([params[:credential_id], params[:permission_id]])
    end

    # Only allow a trusted parameter "white list" through.
    def credentials_permission_params
      params.require(:credentials_permission).permit(:credential_id, :permission_id)
    end
end
