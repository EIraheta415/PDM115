class ApplicationController < ActionController::API

  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response
  rescue_from ActiveRecord::RecordNotUnique, with: :render_not_unique_response

  def render_unprocessable_entity_response(exception)
    render json: { error: exception.message } , status: :unprocessable_entity
  end

  def render_not_found_response(exception)
    render json: { error: exception.message }, status: :not_found
  end

  def render_not_unique_response(exception)
    render json: { error: 'Record already exists' }, status: :not_found
  end

  #For the user to be used across all controllers


  attr_reader :current_user

  private

  def  verify_cred_admin
    extract_token
    if(@current_user && !@current_user.permissions.where(permission_name: 'credential_admin').first)
      render json: {error: 'Forbidden'}, status: 403
    end
  end

  def verify_course_admin
    if(@current_user && !@current_user.permissions.where(permission_name: 'course_admin').first)
      render json: {error: 'Forbidden'}, status: 403
    end
  end



  def extract_token
    if request.headers['Authorization'].present?
     @token = request.headers['Authorization'].split(' ').last
     command = AuthorizeApiRequest.call(@token)

     if command.success?
       @current_user = command.result
     else
       render json: {error: 'Invalid token'}, status: 401
     end

    else
      render json: {error: 'No token in headers'}
    end
  end




end
