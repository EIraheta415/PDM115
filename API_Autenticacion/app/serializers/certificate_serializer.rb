class CertificateSerializer < ActiveModel::Serializer
  attributes :id, :token, :credential_id, :date_certificate
end
