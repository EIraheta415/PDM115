class PermissionSerializer < ActiveModel::Serializer
  attributes :id, :permission_name
end
