class CredentialSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :dui, :municipality
end
