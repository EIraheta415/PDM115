class Credential < ApplicationRecord

  #belongs_to :person
  before_create :generate_token
  has_secure_password
  has_many :credentials_permissions
  has_many :permissions, :through => :credentials_permissions

  def generate_token
    self.confirmation_token = SecureRandom.hex(64)
  end

end
