class CredentialsPermission < ApplicationRecord
  self.primary_keys = :credential_id, :permission_id
  belongs_to :credential
  belongs_to :permission
end
