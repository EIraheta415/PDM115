class Permission < ApplicationRecord
  has_many :credentials_permissions
  has_many :credentials, :through => :credentials_permissions
end
