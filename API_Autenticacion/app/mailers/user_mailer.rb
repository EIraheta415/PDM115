class UserMailer < ActionMailer::Base
  default :from => 'pseesnoreply@gmail.com'
  layout "mailer"
  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email(email, token)
    @email = email
    @token = token
    mail( :to => @email,
    :subject => 'Confirma tu cuenta de PSEES' )
  end

  def send_password_reset(email, id, token)
    @email = email
    @id = id
    @token = token
    mail( :to => @email,
    :subject => 'Cambio de contraseña de tu cuenta PSEES' )
  end

end
