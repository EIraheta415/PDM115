class JsonWebToken
  class << self

    def encode(payload, secret, exp=nil)
      payload[:exp] = exp.hours.from_now.to_i if !exp.nil?
      JWT.encode(payload, secret)
    end

    def decode(token, secret)
      body = JWT.decode(token, secret)[0]
      HashWithIndifferentAccess.new body
    rescue
      nil
    end

  end
end
