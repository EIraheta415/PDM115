# Authentication API for PSEES Services

This API provides resources for all the other PEES apis to guarantee that the information is provided to a real user, achieved by
returning json web tokens that authenticate the user and provide information about the courses he completed.


* Ruby on rails version: 5.0.2

* Ruby version: 2.4.0p0

* Gems used:

  * [Bcrypt](https://github.com/codahale/bcrypt-ruby)

  * [JWT](https://github.com/jwt/ruby-jwt)

  * [SimpleCommand](https://github.com/nebulab/simple_command)

# API Dictionary

Host: https://pseesapiauth.herokuapp.com/

## Routes

### Credentials

Attributes: id, email, password_digest, person_id


* POST */credentials*

  * takes:
  ```
  {"email":"email_1", "password":"pass1", "password_confirmation":"pass1"}
  ```
  * responds:
  ```
  {"id":"id_1", "email":"email_1"}
  ```

* GET */credentials*
  * takes:
    ```
    {""}
    ```

  * responds:
  ```
    [{"id":"id_1", "email":"email_1"}, {"id":"id_2", "email":"email_2"},...]
  ```  


* GET */credentials/:id* (Any natural number)
  * takes:
    ```
    {""}
    ```

  * responds:
  ```
  {"id":"", "email":""}
  ```

* PUT | PATCH */credentials/:id* (Any natural number)
  * takes:
  ```
  {"email":""} //Any parameter to update
  ```
  * responds:
  ```
  {"id":"", "email":""}
  ```

* DELETE */credentials/:id* (Any natural number)
  * takes:
  ```
  {""}
  ```
  responds:
  ```
  {""}
  ```

### People

Attributes: first_name, last_name, dui, municipality


* POST */people*

    A person creation involves having credentials before hand. Every creation of a person must take a user email, this related to credentials present in the database.
    * takes:
    ```
    {"first_name":"name", "last_name":"lastname", "dui":"05380881-3", "municipality":"City", "user_email":"mail@mail.com"}
    ```
    * responds:
    ```
  {"first_name":"name", "last_name":"lastname", "dui":"05380881-3", "municipality":"City"}
    ```

* GET */people*
    * takes:
      ```
      {""}
      ```

    * responds:
    ```
      [{"first_name":"name1", "last_name":"lastname1", "dui":"dui_1", "municipality":"city_1"}, {"first_name":"name2", "last_name":"lastname2", "dui":"dui_2", "municipality":"city_2"},...]
    ```


* GET */people/:id* (Any natural number)
    * takes:
      ```
      {""}
      ```

    * responds:
    ```
    {"first_name":"name", "last_name":"lastname", "dui":"05380881-3", "municipality":"City"}
    // or HTTP 404
    ```

* PUT | PATCH */people/:id* (Any natural number)

  Allowed parameters:
    *first_name*,
    *last_name*,
    *dui*,
    *municipality*,

    * takes:
    ```
    {"parameter":"param_value"} //Any parameters to update from the allowed list
    ```
    * responds:
    ```
    {"first_name":"name", "last_name":"lastname", "dui":"05380881-3", "municipality":"City"}
    ```

* DELETE */people/:id* (Any natural number)
    * takes:
    ```
    {""}
    ```
    responds:
    ```
    {""}
    //or HTTP 404
    ```

### Certificates

Attributes: token, date_certificate

* POST */certificates*

  Certificate creation requires the existence of credentials
    * takes:
      ```
      {"email":"mail@mail.com", "organization":"nameoforg", "course":"nameofcourse"}
      ```
    * responds:
      ```
      {"id":"id_1", "token":"encryptedJWT", "credential_id":"id", "date_certificate":"date"}
      ```

* GET */certificates*
    * takes:
      ```
      {""}
      ```

    * responds:
      ```
      [{"id":"id_1", "token":"encryptedJWT1", "credential_id":"id1", "date_certificate":"date1"},
      {"id":"id_2", "token":"encryptedJWT2", "credential_id":"id2", "date_certificate":"date2"},...]
      ```


* GET */certificates/:id* (Any natural number)
    * takes:
      ```
      {""}
      ```

    * responds:
     ```
     {"id":"id_1", "token":"encryptedJWT", "credential_id":"id",  "date_certificate":"date"}
     // or HTTP 404
     ```

* DELETE */certificates/:id* (Any natural number)
    * takes:
    ```
    {""}
    ```
    * responds:
    ```
    {""}
    //or HTTP 404
    ```

### Permissions

Attributes: id, permission_name


* POST */permissions*

    * takes:
    ```
    {"permission_name":"name"}
    ```
    * responds:
    ```
    {"permission_name":"name"}
    ```

* GET */permissions*
    * takes:
      ```
      {""}
      ```

    * responds:
    ```
      [{"id":"permission_id1", "permission_name":"perm_name1"},
      {"id":"permission_id2", "permission_name":"perm_name2"},...]
    ```


* GET */permissions/:id* (Any natural number)
  * takes:
      ```
      {""}
      ```

  * responds:
    ```
    {"id":"permission_id", "permission_name":"perm_name"}
    // or HTTP 404
    ```

* PUT | PATCH */permissions/:id* (Any natural number)

  Allowed parameters:
    *permission_name*

    * takes:
     ```
     {"name_of_parameter":"param_value"} //Any parameters to update from the allowed list
     ```
    * responds:
     ```
     {"id":"permission_id", "permission_name":"perm_name"} //The updated permission
     ```

* DELETE */permissions/:id* (Any natural number)
    * takes:
     ```
     {""}
     ```
    * responds:
     ```
     {""}
     //or HTTP 404
     ```

### Assign permissions

* POST */grant_permission*

    * takes:
      ```
      {"credential_id":"cred_id", "permission_id":"perm_id"}
      ```
    * responds:
      ```
      {"credential_id":"cred_id", "permission_id":"perm_id"} //with HTTP 200
      ```

* GET */credentials/:id_credential/permissions/* (:id_credential is any natural number)
The list of permissions this user has been granted

    * takes:
        ```
        {""}
        ```

    * responds:
      ```
      [{"id":"permission_id1", "permission_name":"perm_name1"},
        {"id":"permission_id2", "permission_name":"perm_name2"},...]
      // or HTTP 404
      ```


* GET */permissions/:id_permission/users/* (:id_credential is any natural number)
 The list of users that were granted this permission
    * takes:
        ```
        {""}
        ```

    * responds:
     ```
      [{"id":"credential_id1", "email":"email1"},
        {"id":"credential_id2", "email":"email2"},...]
      // or HTTP 404
        ```

* DELETE */credentials/:id_credential/permissions/:id_permission* (Any natural number for both ids)
Used to remove one permission from a user
    * takes:
      ```
      {""}
      ```
    * responds:
      ```
      {""} // with HTTP 200
      //or HTTP 404 if it's already gone
      ```

### Miscellaneous

* POST */authenticate*

    * takes:
      ```
      {"email":"user_email", "password":"the_password"}
      ```
    * responds:
      ```
      {"token":"encryptedToken"} //with HTTP 200
      // or HTTP 404 if no user was found or password does not match credential email

      ```

* GET */certificate/check?certificate=:token*

    * takes:
      ```
      {""}
      ```
    * responds:
      ```
      {"email":"mail@mail.com", "organization":"nameoforg", "course":"nameofcourse"} //with HTTP 200
      // or HTTP 404 if not a valid certificate with error message {"error":"Not a valid certificate"}

      ```

* GET */check?token=:token*

    * takes:
      ```
        {""}
      ```
    * responds:
      ```
      {
        "id": id,
        "email": "sample@mail.com",
        "permissions": [{
          "id": 1,
          "permission_name": "grant_certificate"
          }, {
        "id": 2,
        "permission_name": "manage_users"
        }]
      }

      ```     

* GET */email_verify/:email/:token*
    * takes:
    ```
    {""}
    ```

    * responds:
    ```
    {"email":"sample@mail", "activated_at":"Date"} //or error {error: 'Token already used, your account should be active'}
    ```

* GET */password_reset/:user_id*

    * takes:
    ```
    {""}
    ```

    * responds
    ```
    {"email": "sample@mail", "state": "Email sent", token: "token"}
    ```

* POST */pass_change/:user_id/:token*

    * takes:
    ```
    {"new_password":"pass", "new_password_confirmation":"pass"}
    ```

    * responds:
    ```
    {"success":"Password changed"} //if error {"error":"This token is invalid"}
    ```
