Rails.application.routes.draw do
  resources :permissions
  #resources :credentials_permissions
  resources :credentials
  resources :certificates



  post 'authenticate', to: 'authentication#authenticate'
  get 'check', to: 'authentication#check'
  get 'signup', to: 'credentials#create'
  get 'certificate/check', to: 'certificates#check'
  get '/credentials/:credential_id/permissions/:permission_id' => 'credentials_permissions#show'
  post '/grant_permission/' => 'credentials_permissions#create'
  get '/credentials/:id/permissions' => 'credentials#show_permissions'
  get '/permissions/:id/users' => 'permissions#show_users'
  get '/email_verify/:email/:token' => 'credentials#verify_email', constraints: { token: /[0-z\.-]+/, email: /[0-z\.]+/ }
  get '/password_reset/:user_id' => 'credentials#password_reset'
  post '/pass_change/:user_id/:token' => 'credentials#password_change', constraints: { token: /[0-z\.-]+/ }

end
