class ProductSerializer < ActiveModel::Serializer
  belongs_to :measure
  belongs_to :category
  attributes :id ,:product_name, :participant_id,:price,:score,:stock,:new,
              :description,:active,:image,:product_catalog_id

              class MeasureSerializer < ActiveModel::Serializer
              attributes :abbreviation
              end

              class CategorySerializer < ActiveModel::Serializer
              attributes :category_name
              end



end
