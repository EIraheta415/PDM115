class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update,:update_image, :destroy,:cambiar_disponible]
  before_action :set_product_anyone, only: [:product_version]
  # GET /products
  def index
    @products = Product.where(active: true) ##devuelve siempre solo los no borrados logicamente

    render json: @products
  end

  # GET /products/1
  def show
    render json: @product
  end

  # POST /products
  def create
@content_type = Mime::Type.lookup_by_extension(File.extname(params[:product_name])[1..-1]).to_s
@image = Paperclip.io_adapters.for("data:#{@content_type};base64,#{params[:image]}")
@image.original_filename=params[:product_name]

  @product = Product.new(category_id: params[:category_id],measure_id: params[:measure_id],product_catalog_id: params[:product_catalog_id],
                  participant_id: params[:participant_id],product_name: params[:product_name],description: params[:description],
                  price: params[:price],stock: params[:stock],available: params[:available], image: @image)

    if @product.save
      render json: {"id": @product.id}
    else
       Rails.logger.info(@product.errors.messages.inspect)
       render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

def update_image
  @content_type = Mime::Type.lookup_by_extension(File.extname(params[:product_name])[1..-1]).to_s
  @image = Paperclip.io_adapters.for("data:#{@content_type};base64,#{params[:image]}")
  @image.original_filename=params[:product_name]

  if @product.update(product_name: params[:product_name], image: @image)
    render json: @product
  else
    render :json => {:errors => @product.errors}, status: 400
  end

end
  # DELETE /products/1

  ##Realiza el borrado logico
  def destroy

    if @product.update(active: false)
      render json: {exito: "Has been deleted"}
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

def product_version
@product_original=@product
products_daughter=[]
products_daughter.push(@product)


while @product.product do

  products_daughter.push(@product.product)
  @product=@product.product
end
@product=@product_original
while @product.father do
  products_daughter.push(@product.father)
  @product=@product.father
end

render json: products_daughter
end

def top
if @top=Product.where(active: true).order('score DESC').first(10)
render json: @top
else
   Rails.logger.info(@product.errors.messages.inspect)
    render json: @product.errors, status: :unprocessable_entity

end

end




def select_por_disponibles

    render json: Product.select_disponibles()    ##devuelve los productos no disponibles
end
def select_from_desactivated
  render json: Product.select_deactivated()     ##devuelve los productos eliminados logicamente
                                                ## no se para que serviria XD
end

def select_filtro                             ## mi feo filtro ..ojala se llege a ocupar XD
  nombre=params[:product_name]
  mayor=params[:mayor]
  menor=params[:menor]
  if mayor=="" || menor ==""
    mayor=Product.maximum("precio").to_s
    menor=Product.minimum("precio").to_s
  end
  @product=Product.where("product_name LIKE '%"+nombre+"%'").where("price <="+mayor+" AND price>="+menor)
  .rewhere(available: params[:available]).rewhere(active: params[:active])
render json: @product
end
###############################################################################################################
      ##Modificar atributos
  # GET /productos/1


  def cambiar_disponible                    ##cambia el estado del producto con respecto al stock
    @product.disponible=!@product.disponible
    @product.update(product_params)
    render json:@product
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
      if !@product.active
        render json: @product.errors, status: :unprocessable_entity
      end
    end
    def set_product_anyone
      @product=Product.find(params[:id])
    end

    def product_params
    params.permit(:category_id,:measure_id,:product_catalog_id,
                    :participant_id,:product_id,:product_name,:new,:description,
                    :stock,:price,:score,:available,:active,:image)
  end


  def product_params_create
    params.permit(:category_id,:measure_id,:product_catalog_id,
                    :participant_id,:product_id,:product_name,:description,
                    :price,:stock,:available)
  end



  def product_params_active
    params.permit(:active)
  end

  def change_active
    @product=Product.find(params[:id])

  end

    # Only allow a trusted parameter "white list" through.

end
