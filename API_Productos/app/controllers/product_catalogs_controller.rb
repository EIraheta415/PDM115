class ProductCatalogsController < ApplicationController
  before_action :set_product_catalog, only: [:show, :update, :destroy, :get_products]
  before_action :set_product_catalog_store, only: [:get_products_store]
  # GET /product_catalogs
  def index
    @product_catalogs = ProductCatalog.all

    render json: @product_catalogs
  end

  # GET /product_catalogs/1
  def show
    render json: @product_catalog
  end

  # POST /product_catalogs
  def create
    @product_catalog = ProductCatalog.new(product_catalog_params)

    if @product_catalog.save
      render json: @product_catalog, only: :id
    else
      render json: @product_catalog.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /product_catalogs/1
  def update
    if @product_catalog.update(product_catalog_params)
      render json: @product_catalog
    else
      render json: @product_catalog.errors, status: :unprocessable_entity
    end
  end

  # DELETE /product_catalogs/1
  def destroy
    @product_catalog.destroy
  end

  def get_products
    render json: @product_catalog.products.where(active: true)

  end

  def get_products_store
    render json: @product_catalog.products.where(active: true)
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_catalog
      @product_catalog = ProductCatalog.find(params[:id])
    end

    def set_product_catalog_store
      @product_catalog = ProductCatalog.find(1)   ## aca extrae el catalogo de la tienda
    end                                    ##suponiendo que es el 1

    # Only allow a trusted parameter "white list" through.
    def product_catalog_params
      params.permit(:catalog_name,:description)
    end
end
