class ParticipantsController < ApplicationController
  before_action :set_participant, only: [:show, :update, :destroy, :get_products,:get_catalog]

  # GET /participants
  def index
    @participants = Participant.all

    render json: @participants
  end

  # GET /participants/1
  def show
    render json: {participant: @participant, account: @participant.credential}
  end

def show_credentials
  @credentials=Credential.all
render json: @credentials
end
  # POST /participants
  def create
    @participant = Participant.new(participant_params)
if @participant.add_credential(params[:email], params[:password], params[:password_confirmation],params[:first_name],params[:last_name],params[:dui],params[:municipality])
    if @participant.save
      render json: @paticipant, status: :created, location: @participant
    else
      render json: @participant.errors, status: :unprocessable_entity
    end
  else
    render json: {message: 'Error creating account'}
  end
  end
  

  # PATCH/PUT /participants/1
  def update
    if @participant.update(participant_params)
      render json: @participant
    else
      render json: @participant.errors, status: :unprocessable_entity
    end
  end

  # DELETE /participants/1
  def destroy
      @participant.destroy
    @participant.delete_credential

  end




def get_products

render json: @participant.products.where(active: true)

end

def get_catalog
render json: @participant.product_catalog.products.where(active: true).order('score DESC')
end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_participant
      @participant = Participant.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def participant_params
      params.permit(:first_name,
      :last_name,:product_catalog_id)
    end


end
