class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :update, :destroy, :get_products]

  # GET /categories
  def index
    @categories = Category.all

    render json: @categories
  end

  # GET /categories/1
  def show
    render json: @category
  end

  # POST /categories
  def create
    @category = Category.new(category_params)

    if @category.save
      render json: @category, only: :id
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /categories/1
  def update
    if @category.update(category_params)
      render json: @category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /categories/1
  def destroy
    @category.destroy
  end


  def get_products
    render json: @category.products.where(active: true)
end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id]).where(id_category: params[:id_catalog])
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.permit(:category_name, :description)
    end
end
