class Product < ApplicationRecord
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_attachment_presence :image


belongs_to :product_catalog
belongs_to :category
belongs_to :measure
belongs_to :participant
belongs_to :product , optional: true
has_one :father, class_name: "Product"
after_initialize :init

validates :score,:stock,:price,numericality: { greater_than_or_equal_to: 0.0 } # solo enteros mayores a 0

# para buscar por catalogo
#para buscar por producto disponible
def self.select_disponibles()
  if @product=Product.where("available="+"true")
    return @product
  else
    render json: @product.errors, status: :unprocessable_entity
  end
end

# para buscar por producto activo
def self.select_deactivated()
  if @product=Product.where("active="+"false")
  return @product
else
  reder json: @product.errors, status: :unprocessable_entity
end
end

def init
  self.score ||=0.0
  
  self.new ||=true
  self.active ||=true
end
def image_url
    image.url(:thumb)
  end


end
