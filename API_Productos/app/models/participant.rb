class Participant < ApplicationRecord
has_many :products
belongs_to :product_catalog
def credential
Credential.find(self.id)
end

  def add_credential(email,password,password_confirmation,first_name,last_name,dui,municipality)
    @credential=Credential.new({email: email, password: password, password_confirmation: password_confirmation,first_name: first_name,last_name: last_name, dui: dui,municipality: municipality})
    if @credential.save
      self.id=@credential.id  ## creo que aca debe ser self.id=@credential.id
    else
      @credential.errors
    end
  end

    def delete_credential
      @cred =Credential.find(self.id)
      @cred.destroy
    end
end
