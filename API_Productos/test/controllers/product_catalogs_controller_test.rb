require 'test_helper'

class ProductCatalogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_catalog = product_catalogs(:one)
  end

  test "should get index" do
    get product_catalogs_url, as: :json
    assert_response :success
  end

  test "should create product_catalog" do
    assert_difference('ProductCatalog.count') do
      post product_catalogs_url, params: { product_catalog: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show product_catalog" do
    get product_catalog_url(@product_catalog), as: :json
    assert_response :success
  end

  test "should update product_catalog" do
    patch product_catalog_url(@product_catalog), params: { product_catalog: {  } }, as: :json
    assert_response 200
  end

  test "should destroy product_catalog" do
    assert_difference('ProductCatalog.count', -1) do
      delete product_catalog_url(@product_catalog), as: :json
    end

    assert_response 204
  end
end
