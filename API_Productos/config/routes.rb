Rails.application.routes.draw do
  resources :roles
  resources :products
  resources :product_catalogs
  resources :participants
  resources :measures
  resources :categories

put 'products/change_image/:id', to: 'products#update_image'
  ##Select deacuerdo a atribulos

get '/desactivated_products', to: 'products#select_from_desactivated'  # selecciona los no activos
get '/disavalilable_products', to: 'products#select_por_disponibles'    #selecciona los disponibles

scope '/getproducts' do
get '/category/:id', to: 'categories#get_products'            #selecciona los productos por category
get '/catalog/:id', to: 'product_catalogs#get_products'       #selecciona los productos por catalog
get '/participant/:id', to: 'participants#get_products'       #selecciona los productos por participantes
get '/catalogstore/find', to: 'product_catalogs#get_products_store'
get '/all_versions/:id', to: 'products#product_version'
end
get '/catalog/:id', to: 'participants#get_catalog'   ## Deacuerdo al id del participante muestra el catalogo donde pertenece
get '/credentials', to: 'participants#show_credentials'

get 'products/top/find', to: 'products#top'
##cambiar atributos
match '/products/cambiar_disponible/:id', to: 'products#cambiar_disponible', via:[:PUT] #cambia el campo disponible
match '/products/filtro', to: 'products#select_filtro', via:[:POST]       #el filtro XD
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
