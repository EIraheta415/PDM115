Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :orders
  resources :order_details
  #****************************************************************************
  #Gets Especiales
  #****************************************************************************
  #Gets de Order
  get '/orders/supplier/:supplier_id', to: 'orders#get_orders_supplier'
  get '/orders/client/:client_id', to: 'orders#get_orders_client'
  get '/orders/supplier/client/:supplier_id/:client_id', to: 'orders#get_orders_supplier_client'
  #****************************************************************************
  #Gets de OrderDetail
  get '/order_details/order/:order_id', to: 'order_details#get_order_details_order'
  get '/order_details_product/order/:order_id', to: 'order_details#get_order_details_order_product'
  get '/order_details/product/:product_id', to: 'order_details#get_order_details_product'
  get '/order_details/product_delivered/:product_id', to: 'order_details#get_order_details_delivered_product'
end
