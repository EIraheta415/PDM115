class OrderDetailSerializer < ActiveModel::Serializer
  attributes :id, :order_id, :product_id, :quantity, :sale_price, :amount, :score, :commentary
end
