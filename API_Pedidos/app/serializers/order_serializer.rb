class OrderSerializer < ActiveModel::Serializer
  attributes :id, :supplier_id, :client_id, :status, :amount, :date_delivery, :created_at
  #has_many :order_details
end
