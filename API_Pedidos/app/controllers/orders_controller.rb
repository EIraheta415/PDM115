class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :update, :destroy]

  # GET /orders
  def index
    @orders = Order.all
    @orders.each do |order|
      validate_status(order)
    end
    render json: @orders
  end

  # GET /orders/1
  def show
    @order= Order.find(params[:id])
    validate_status(@order)
    render json: @order
  end

  # GET /orders/supplier/1
  def get_orders_supplier
    @order= Order.where("supplier_id = ?", params[:supplier_id])
    render json: @order
  end

  # GET /orders/client/1
  def get_orders_client
    @order= Order.where("client_id = ?", params[:client_id])
    render json: @order
  end

  # GET /orders/supplier/client/1/1
  def get_orders_supplier_client
    @order= Order.where("supplier_id = ? AND client_id = ?",params[:supplier_id], params[:client_id])
    render json: @order
  end

  # POST /orders
  def create
    client_id=params[:client_id].to_i
    product_list=params[:product_list][1,params[:product_list].length-2].split(",").map { |s| s.to_i }
    quantity_list=params[:quantity_list][1,params[:quantity_list].length-2].split(",").map { |s| s.to_f }
    date_delivery=Date.parse params[:date_delivery]
    supplier_list=[]
    order_list=[]

    if validate_attributes==0 #No hay errores lógicos en los parámetros
      product_list.each_with_index do |product_id, i|
        product=Product.find(product_id)
        if supplier_list.empty?
          supplier_list.push(product.participant_id)
          order=Order.new supplier_id: product.participant_id, client_id: client_id, date_delivery: date_delivery, status: "Solicitado"
          orderDetail=OrderDetail.new product_id: product.id, quantity: quantity_list[i], sale_price: product.price.to_f, amount: quantity_list[i].to_f * product.price.to_f
          order_list.push(order)
          order_list.push([])
          order_list[-1].push(orderDetail)
        else
          b=0
          j=0
          while b==0 && j < supplier_list.length
            if supplier_list[j] == product.participant_id
              orderDetail=OrderDetail.new product_id: product.id, quantity: quantity_list[i], sale_price: product.price.to_f, amount: quantity_list[i] * product.price.to_f
              order_list[2*j+1].push(orderDetail)
              b=1 #Se sale del ciclo cuando ya ha guardado
            end
            j+=1
          end
          if b==0 #Si no se encontró proveedor
            supplier_list.push(product.participant_id)
            order=Order.new supplier_id: product.participant_id, client_id: client_id, date_delivery: date_delivery, status: "Solicitado"
            orderDetail=OrderDetail.new product_id: product.id, quantity: quantity_list[i], sale_price: product.price.to_f, amount: quantity_list[i] * product.price.to_f
            order_list.push(order)
            order_list.push([])
            order_list[-1].push(orderDetail)
          end
        end
      end
      order_saved_list=[]
      order_list.each_with_index do |item, i|
        if i%2==0 #Es un Pedido
          @order=item
          @order.save
          order_saved_list.push(@order)
          create_message(@order.id)
        else #Es una lista de Detalles de Pedido
          acum=0
          item.each_with_index do |detail, j|
            acum+=detail.amount
            order_list[i][j].order_id=@order.id
            detail=OrderDetail.create order_list[i][j].attributes #Retorna un hash con todos los atributos del objeto OrderDetail en la lista
            order_list[i][j].id=detail.id
          end
          order=order_list[i-1]
          order.update(amount: acum)
          order_list[i-1].amount=acum #Actualizando lista
        end
      end
      render json: order_saved_list
    else
      render json: {error: 'Error en los parámetros'}
    end
  end

  # PATCH/PUT /orders/1
  def update
    b=0
    status=params[:status]
    if status=="Solicitado" || status=="Aceptado" || status=="Denegado" || status =="Entregado" || status=="Expirado"
      detail_list=OrderDetail.where("order_id = ?", @order.id)
      if @order.status=="Solicitado" && status=="Aceptado" #Solicitado -> Aceptado
        if @order.update(status: status)
          #Actualizando las existencias en los productos
          detail_list.each do |detail|
            product=Product.find(detail.product_id.to_i)
            product.stock=product.stock.to_f-detail.quantity
            product.save
          end
        else
          b=1
          error="Error al actualizar"
        end
      else
        if @order.status=="Aceptado" && status=="Expirado" #Aceptado -> Expirado
          if @order.update(status: status)
            #Actualizando las existencias en los productos
            detail_list.each do |detail|
              product=Product.find(detail.product_id.to_i)
              product.stock=product.stock.to_f+detail.quantity
              product.save
            end
          else
            b=1
            error="Error al actualizar"
          end
        else
          if (@order.status=="Solicitado"  && (status=="Denegado" || status=="Expirado")) ||  (@order.status=="Aceptado" && status=="Entregado")#Solicitado -> Denegado o Expirado
            if @order.update(status: status)
            else
              b=1
              error="Error al actualizar"
            end
          else
            error="Error. El estado especificado no puede actualizar al actual."
            b=1
          end
        end
      end
    else
      error="Error! Estado no válido"
      b=1
    end

    if b==0
      render json: @order
    else
      render json: {error: error}
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.fetch(:order, {})
    end

    def validate_status(order)
      if(Time.now.to_f > order.date_delivery.to_f && (order.status != "Entregado" && order.status != "Denegado"))
        order.status="Expirado"
        order.save
      end
    end

    def create_message (order_id)
      orderMessage = OrderMessage.new(order_id:order_id,message:'Hola recibi tu pedido, cualquier consulta estoy a tu servicio.',route: true)
      orderMessage.save
    end

    def validate_attributes
      b=0 #Los parámetros no contienen errores.
      quantity_list=params[:quantity_list][1,params[:quantity_list].length-2].split(",").map { |s| s.to_f }
      date_delivery=Date.parse params[:date_delivery]
      quantity_list.each do |quantity|
        if quantity <= 0
          b=1 #Ninguna cantidad debe ser menor o igual que cero.
        end
      end
      if date_delivery <= Time.now
        b=1 #La fecha de entrega no debe ser menor o igual a la actual.
      end
      return b
    end
end
