class OrderDetailsController < ApplicationController
  before_action :set_order_detail, only: [:show, :update, :destroy]

  # GET /order_details
  def index
    @order_details = OrderDetail.all

    render json: @order_details
  end

  # GET /order_details/1
  def show
    render json: @order_detail
  end

  # GET /order_details_product/order/1
  def get_order_details_order_product
    @order_details= OrderDetail.where("order_id = ?", params[:order_id])
    product_name_list=[]
    @order_details.each do |order|
      product_name_list.push(Product.find(order.product_id).product_name)
    end
    render json: {detalles: @order_details, product_name_list: product_name_list}
  end

  # GET /order_details/order/1
  def get_order_details_order
    @order_details= OrderDetail.where("order_id = ?", params[:order_id])

    render json: @order_details
  end
  # GET /order_details/product/1
  def get_order_details_product
    @order_details= OrderDetail.where("product_id = ?", params[:product_id])
    render json: @order_details
  end

  # GET /order_details/product_delivered/1
  def get_order_details_delivered_product
    @order_details =[]
    detail_list = OrderDetail.where("product_id = ?", params[:product_id])
    detail_list.each do |detail|
      if Order.find(detail.order_id).status == "Entregado"
        @order_details.push(detail)
      end
    end
    render json: @order_details
  end

  # POST /order_details
  def create
    @order_detail = OrderDetail.new(order_detail_params)

    if @order_detail.save
      render json: @order_detail, status: :created, location: @order_detail
    else
      render json: @order_detail.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /order_details/1
  def update
    error=0
    if params[:score]
      if @order_detail.update(score: params[:score])
      else #Si da error
        error=1
      end
    end
    if params[:commentary]
      if @order_detail.update(commentary: params[:commentary])
      else #Si da error
        error=1
      end
    end
    if error=0
      render json: @order_detail
    else
      render json: @order_detail.errors, status: :unprocessable_entity
    end
  end

  # DELETE /order_details/1
  def destroy
    @order_detail.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_detail
      @order_detail = OrderDetail.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_detail_params
      params.fetch(:order_detail, {})
    end
end
