class Order < ApplicationRecord
  belongs_to :supplier, :class_name => 'Participant', :foreign_key => 'supplier_id'
  belongs_to :client, :class_name => 'Participant', :foreign_key => 'client_id'

  has_many :order_messages
  has_many :order_details
end
