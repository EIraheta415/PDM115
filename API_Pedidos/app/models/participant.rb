class Participant < ApplicationRecord
  has_many :products
  has_many :received_order, :class_name => 'Order', :foreign_key => 'supplier_id'
  has_many :realized_order, :class_name => 'Order', :foreign_key => 'client_id'
end
