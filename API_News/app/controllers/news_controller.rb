class NewsController < ApplicationController
  #before_action :course_admin, except: [:show]
  #before_action :any_user, only: [:show]
  # GET /news/1

  def show_all
    @news = News.all
    render json: @news
  end

  def show
    @news = News.find(params[:id])
    render json: @news
  end

  # POST /news
  def create
    content_type = Mime::Type.lookup_by_extension(File.extname(params[:image_file_name])[1..-1]).to_s
    # Creates a new paperclip file, decoding the params.
    image = Paperclip.io_adapters.for("data:#{content_type};base64,#{params[:image_encoded]}")
    # Set the file's name.
    image.original_filename = params[:image_file_name]

    @news = News.new(section_id: params[:section_id], course_id: params[:course_id],news_title: params[:news_title], news_content: params[:news_content], news_autor: params[:news_autor], image: image)
    if(@news.save)
        render :json => @news
    else
      render :json => {:errors => ["Something went wrong"]}, status: 400
    end
  end

  # PATCH/PUT /news/1
  def update_new
    @news = News.find(params[:id])
    begin
      if params[:section_id].blank? == false && params[:section_id] != @news.section_id
        @news.update_attribute(:section_id, params[:section_id])
      end
      if params[:course_id].blank? == false && params[:course_id] != @news.course_id
        @news.update_attribute(:course_id, params[:course_id])
      end
      if params[:news_title].blank? == false && params[:news_title] != @news.news_title
        @news.update_attribute(:news_title, params[:news_title])
      end
      if params[:news_content].blank? == false && params[:news_content] != @news.news_content
        @news.update_attribute(:news_content, params[:news_content])
      end
      if params[:news_autor].blank? == false && params[:news_autor] != @news.news_autor
        @news.update_attribute(:news_autor, params[:news_autor])
      end
      if params[:image_file_name].blank? == false && params[:image_file_name] != @news.image_file_name
        update_image()
      else
        render :json => {:success => ["All ok!"]}, status: 200
      end
    end
  end

  # DELETE /news/1
  def destroy
    begin
      @news = News.find(params[:id])
      @news.destroy
      render :json => {:success => ["News deleted!"]}, status: 200
    rescue ActiveRecord::RecordNotFound
      render :json => {:error => ["News not found"]}, status: 404
    end
  end

  private
    # Only allow a trusted parameter "white list" through.

    def update_image
        content_type = Mime::Type.lookup_by_extension(File.extname(params[:image_file_name])[1..-1]).to_s
        # Creates a new paperclip file, decoding the params.
        image = Paperclip.io_adapters.for("data:#{@content_type};base64,#{params[:image_encoded]}")
        # Set the file's name.
        image.original_filename = params[:image_file_name]
        to_update = News.find(params[:id])
        if to_update.update_attribute(:image, image)
          render :json => {:success => ["All updated!"]}, status: 200
        else
          render :json => {:errors => ["Something went wrong"]}, status:400
        end
    end

    def news_params
      params.require(:news).permit(:section_id, :course_id, :news_title, :news_content, :news_autor)
    end
end
