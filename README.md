# Proyecto de Solidaridad Económica de El Salvador (PSEES)

Proyecto para la materia **PDM115**, que se divide en *3 subsistemas*:


### Virtual Sol:
Sitio web educativo sobre la economía solidaria.
### Mercaredes:
Aplicación móvil para la red de productores de una localidad
### Mercadomo torogoz:
Tienda virtual, compuesta de un sitio web para la administración y aplicación móvil para los clientes.
