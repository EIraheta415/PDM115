class MaterialType < ApplicationRecord
  has_attached_file :image, styles: { thumb: '48x48>' }
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_attachment_presence :image

  validates :material_type_name, presence: true

  def image_url
    image.url(:thumb)
  end
end
