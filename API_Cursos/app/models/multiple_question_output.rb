class MultipleQuestionOutput
  # Getters and setters.
  attr_accessor :question, :correct_answers, :incorrect_answers

  # Constructor
  def initialize(question, correct_answers, incorrect_answers)
    @question = question    # A Statement object or a string.
    @correct_answers = correct_answers  # A Statement array or a string array. 
    @incorrect_answers = incorrect_answers  # A Statement array or a string array.
  end
end
