class SimpleQuestion
  # Getters and setters.
  attr_accessor :evaluation_id, :statement, :answer_correct, :answers_incorrects

  # Constructor
  def initialize(evaluation_id, statement, answer_correct, answers_incorrects)
    @evaluation_id = evaluation_id
    @statement = statement
    @answer_correct = answer_correct
    @answers_incorrects = answers_incorrects
  end

  # Method "valide?" determinates wheter or not it's a valid question structure.
  def valide?
    @evaluation_id && @statement &&
      !(@answer_correct.kind_of?(Array)) && @answers_incorrects.kind_of?(Array) &&
      @answers_incorrects.size == 3
  end
end
