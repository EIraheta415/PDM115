class CourseModule < ApplicationRecord
  has_one :evaluation, dependent: :destroy
  has_many :study_material, dependent: :destroy

  belongs_to :course

  validates :course, :module_name, :module_objective, :module_level, presence: true
end
