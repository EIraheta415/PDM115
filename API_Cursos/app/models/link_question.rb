class LinkQuestion
  # Getters and setters.
  attr_accessor :evaluation_id, :questions

  # Constructor
  def initialize(evaluation_id, questions)
    @evaluation_id = evaluation_id
    @questions = questions
  end

  def add_question(question)
    @questions.push(question)
    valide?
  end

  # Method "valide?" determinates wheter or not it's a valid question structure.
  def valide?
    @evaluation_id && @questions && @questions.kind_of?(Array) &&
      @questions.size > 0 && valide_questions?
  end

  def valide_questions?
    @questions.find{ |question|
      !question[:statement] || !question[:answer_correct] || !question[:answers_incorrects] || !question[:answers_incorrects].kind_of?(Array)
    }.nil?
  end # end def

  private :valide_questions?
end
