class Question < ApplicationRecord
  # Adding relation (Allows on cascade deletion)
  has_many :answer, dependent: :destroy

  def correct_answers_count
    Answer.where(question_id: self.id, correct_answer: true).count
  end

  def incorrect_answers_count
    Answer.where(question_id: self.id, correct_answer: false).count
  end
end
