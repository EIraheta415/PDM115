class Statement
  # Getters and setters.
  attr_accessor :id, :statement

  # Constructor
  def initialize(id, statement)
    @id = id    # the id of a DB row, for tables "questions" or "answers"
    @statement = statement
  end
end
