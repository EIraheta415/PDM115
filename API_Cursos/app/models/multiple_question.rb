class MultipleQuestion
  # Getters and setters.
  attr_accessor :evaluation_id, :statement, :answers_corrects, :answers_incorrects

  # Constructor
  def initialize(evaluation_id, statement, answers_corrects, answers_incorrects)
    @evaluation_id = evaluation_id
    @statement = statement
    @answers_corrects = answers_corrects
    @answers_incorrects = answers_incorrects
  end

  # Method "valide?" determinates wheter or not it's a valid question structure.
  def valide?
    @evaluation_id && @statement &&
      @answers_corrects.kind_of?(Array) && @answers_corrects.size > 0 &&
      @answers_incorrects.kind_of?(Array) && @answers_incorrects.size > 0
  end
end
