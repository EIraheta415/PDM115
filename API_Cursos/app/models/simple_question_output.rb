class SimpleQuestionOutput
  # Getters and setters.
  attr_accessor :question, :correct_answer, :incorrect_answers

  # Constructor
  def initialize(question, correct_answer, incorrect_answers)
    @question = question  # A Statement object or a string.
    @correct_answer = correct_answer  # A Statement object or a string.
    @incorrect_answers = incorrect_answers  # A Statement arrat or a string. 
  end
end
