class Course < ApplicationRecord
  has_many :course_module

  validates :course_name, :course_objective, presence: true
end
