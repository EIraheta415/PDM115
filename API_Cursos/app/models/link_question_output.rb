class LinkQuestionOutput
  # Getters and setters.
  attr_accessor :correlative, :questions

  # Constructor
  def initialize(correlative)
    @correlative = correlative  # Correlative of link question ("questions"."link_question_correlative") 
    @questions = []   # The "questions" attr is an array of SimpleQuestionOutput
  end
end
