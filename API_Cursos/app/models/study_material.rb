class StudyMaterial < ApplicationRecord
  belongs_to :course_module
  belongs_to :material_type

  has_attached_file :material
  validates_attachment_content_type :material, :content_type => ["image/jpg",
                                                              "image/jpeg",
                                                              "image/png",
                                                              "image/gif",
                                                              "application/pdf",
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document", # .docx
                                                           "application/msword", # .doc
    "application/vnd.openxmlformats-officedocument.presentationml.presentation", # .pptx
                                                     "application/mspowerpoint", # .ppt
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", # .xlsx
                                                          "application/msexcel", # .xls
                                      "application/vnd.oasis.opendocument.text", # .odt
                              "application/vnd.oasis.opendocument.presentation", # .odp
                               "application/vnd.oasis.opendocument.spreadsheet"  # .ods
                                                            ]
  validates_attachment_presence :material

  validates :course_module_id, :material_type_id, :study_material_name, presence: true

  def material_file_url
    material.url(:original)
  end
end
