class Evaluation < ApplicationRecord
  belongs_to :course_module

  validates :course_module, presence: true

  # Method: simple
  # Output: return an array with the simple questions asociated to the evaluation.
  # Params:
  #   with_ids: indicates if the output will contain the id of the questions and answers rows (for edition)
  def simple(with_ids)
    @simple = []    # The response object, an array of SimpleQuestionOutput

    # Gets the questions from the DB asociated with the evaluation and type simple.
    @questions = Question.where(evaluation_id: self.id, question_type: "simple")

    # foreach q in questions:
    @questions.each do |q|

      # Gets the answers of the question, separates the correct one and groups the incorrects.
      @answers = Answer.where(question_id: q.id)
      @answer_correct = @answers.where(correct_answer: true).first
      @answers_incorrects = @answers.where(correct_answer: false)

      # If ids were requested.
      if(with_ids)
        # Creates a Statement object for the question.
        @question = Statement.new(q.id, q.question_statement)

        # Creates a Statement object for the correct answer.
        @correct_statement = Statement.new(@answer_correct.id, @answer_correct.answer_statement)

        # Creates an array of Statement for the incorrect answers.
        @incorrect_statements = []
        @answers_incorrects.each do |answer_inc|
          @incorrect_statements.push(Statement.new(answer_inc.id, answer_inc.answer_statement))
        end

      # If ids weren't requested.
      else
        # Sets the strings: question, correct_statement and incorrect_statements[]
        @question = q.question_statement

        @correct_statement = @answer_correct.answer_statement

        @incorrect_statements = []
        @answers_incorrects.each do |answer_inc|
          @incorrect_statements.push(answer_inc.answer_statement)
        end

      end # end of: if with_ids

      # Adds to the response object a new SimpleQuestionOutput object.
      @simple.push(SimpleQuestionOutput.new(@question, @correct_statement, @incorrect_statements))

    end # end of: each question.

    # returns
    @simple

  end # end of: define simple


  # Method: multiple
  # Output: return an array with the multiple questions asociated to the evaluation.
  # Params:
  #   with_ids: indicates if the output will contain the id of the questions and answers rows (for edition)
  def multiple(with_ids)
    @multiple= [] # The response object, an Array of MultipleQuestionOutput

    # Gets the questions from the DB asociated with the evaluation and type multiple.
    @questions = Question.where(evaluation_id: self.id, question_type: "multiple")

    # foreach q in questions.
    @questions.each do |q|

      # Gets the answers of the question, and groups the corrects and the incorrects.
      @answers = Answer.where(question_id: q.id)
      @answers_corrects = @answers.where(correct_answer: true)
      @answers_incorrects = @answers.where(correct_answer: false)

      # If ids were requested.
      if(with_ids)
        # Creates a Statement object for the question.
        @question = Statement.new(q.id, q.question_statement)

        # Creates a Statement array for the correct answers.
        @correct_statements = []
        @answers_corrects.each do |answer_c|
          @correct_statements.push(Statement.new(answer_c.id, answer_c.answer_statement))
        end

        # Creates a Statement array for the incorrect answers.
        @incorrect_statements = []
        @answers_incorrects.each do |answer_inc|
          @incorrect_statements.push(Statement.new(answer_inc.id, answer_inc.answer_statement))
        end

      # If ids weren't requested.
      else
        # Sets the strings: question, correct_statement[] and incorrect_statements[]
        @question = q.question_statement

        @correct_statements = []
        @answers_corrects.each do |answer_c|
          @correct_statements.push(answer_c.answer_statement)
        end

        @incorrect_statements = []
        @answers_incorrects.each do |answer_inc|
          @incorrect_statements.push(answer_inc.answer_statement)
        end

      end # end of: if with_ids

      # Adds to the response object a new SimpleQuestionOutput object.
      @multiple.push(MultipleQuestionOutput.new(@question, @correct_statements, @incorrect_statements))

    end # end of: each question

    @multiple
  end # end of: difene multiple

  # Method: link
  # Output: return an array with the link questions asociated to the evaluation.
  # Params:
  #   with_ids: indicates if the output will contain the id of the questions and answers rows (for edition)
  def link(with_ids)
    @link = []  # The response object. Could by an array of LinkQuestionOutput or an array of SimpleQuestionOutput arrays

    # Gets the all the link questions asociated with the evaluation.
    @questions = Question.where(evaluation_id: self.id, question_type: "link")

    # Gets all the diferent correlatives.
    @link_questions_correlatives = @questions.select(:link_question_correlative).distinct

    # foreach correlative in link_questions_correlatives
    @link_questions_correlatives.each do |correlative|

      # Sets the correlative in question_correlative
      @question_correlative = correlative.link_question_correlative

      # Chooses the type ot the response object
      if(with_ids)
        # An array of LinkQuestionOutput, creates link_questions as a LinkQuestionOutput object
        @link_question = LinkQuestionOutput.new(@question_correlative)
      else
        # An array of SimpleQuestionOutput arrays, creates link_question as an array.
        @link_question = []
      end

      # foreach q in questions asociated with the correlative.
      @questions.where(link_question_correlative: @question_correlative).each do |q|

        # Gets the answers of the question, separates the correct one and groups the incorrects.
        @answers = Answer.where(question_id: q.id)
        @answer_correct = @answers.where(correct_answer: true).first
        @answers_incorrects = @answers.where(correct_answer: false)

        # if the ids were requested
        if(with_ids)
          # Creates a Statement object for the question.
          @question = Statement.new(q.id, q.question_statement)

          # Creates a Statement object for the correct answer.
          @correct_statement = Statement.new(@answer_correct.id, @answer_correct.answer_statement)

          # Creates an array of Statement for the incorrect answers.
          @incorrect_statements = []
          @answers_incorrects.each do |answer_inc|
            @incorrect_statements.push(Statement.new(answer_inc.id, answer_inc.answer_statement))
          end

          # Adds a new SimpleQuestionOutput to the questions of the link_question
          @link_question.questions.push(SimpleQuestionOutput.new(@question, @correct_statement, @incorrect_statements))

        # if the ids weren't requested
        else
          # Sets the strings: question, correct_statement and incorrect_statements[]
          @question = q.question_statement

          @correct_statement = @answer_correct.answer_statement

          @incorrect_statements = []
          @answers_incorrects.each do |answer_inc|
            @incorrect_statements.push(answer_inc.answer_statement)
          end

          # Adds a new SimpleQuestionOutput to link_question
          @link_question.push(SimpleQuestionOutput.new(@question, @correct_statement, @incorrect_statements))

        end # end of: if with_ids

      end # end of: each question

      @link.push(@link_question)
    end # end of: each correlative

    # return
    @link
  end

end
