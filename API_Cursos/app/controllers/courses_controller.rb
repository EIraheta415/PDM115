class CoursesController < ApplicationController
  # Validates user
  before_action :course_admin, only: [:create, :update, :destroy]

  # Executes this action before the specified methods
  before_action :set_course, only: [:show, :update, :destroy]

  # get /courses
  def index
    @courses = Course.all
    render :json => @courses
  end

  # get /courses/:id
  # get /courses/:id?include=modules
  def show
    if(params[:include] == "modules")
      render :json => @course, include: {course_module: {except: :course_id}}, except: :id
    else
      render :json => @course, except: :id
    end
  end

  # post /courses
  def create
    @course = Course.new(course_create_params)

    if @course.save
      render :json => @course, only: :id
    else
      render :json => {:errors =>@course.errors}, status: 400
    end
  end

  # put /courses/:id
  def update
    if @course.update(course_update_params)
      render :json => {:errors => :null}
    else
      render :json => {:errors => @course.errors}, status: 400
    end
  end

  # delete /courses/:id
  def destroy
    @course.destroy
    render :json => {:errors => :null}, status: 200
  end

  # Defining before_action
  def set_course
    @course = Course.find(params[:id])
  end

  # Creation params
  def course_create_params
    params.require(:course).permit(:course_name, :course_objective)
  end

  # Updation params
  def course_update_params
    params.require(:course).permit(:id, :course_name, :course_objective)
  end

  # Declaring private methods.
  private :set_course, :course_create_params, :course_update_params
end
