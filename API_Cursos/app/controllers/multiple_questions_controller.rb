class MultipleQuestionsController < ApplicationController
  # Validates user.
  before_action :course_admin

  #  post /evaluations/:evaluation_id/questions/multiple
  # Creates a new multiple question.
  def create
    # Checks if evaluation exists.
    @evaluation = Evaluation.find(params[:evaluation_id])

    # Creates the multiple question object.
    @multiple_question = MultipleQuestion.new(params[:evaluation_id], params[:statement],
                          params[:answers_corrects], params[:answers_incorrects])

    # If the question is valid (has a statement, at least correct answer and at least an incorrect answer)
    if(@multiple_question.valide?)
      # Stores a row in the table "questions", with type 'multiple'
      @question = Question.new(evaluation_id: @multiple_question.evaluation_id,
                    question_statement: @multiple_question.statement, question_type: "multiple")
      @question.save

      # Stores a row in the table "answers" foreach correct answer
      @multiple_question.answers_corrects.each do |answer_c|
        @answer_correct = Answer.new(question_id: @question.id, answer_statement: answer_c, correct_answer: true)
        @answer_correct.save
      end

      # Stores a row in the table "answers" foreach incorrect answer
      @multiple_question.answers_incorrects.each do |answer_inc|
        @answer_incorrect = Answer.new(question_id: @question.id, answer_statement: answer_inc, correct_answer: false)
        @answer_incorrect.save
      end

      # Returns the id of the new "question" row
      render :json => {:id => @question.id}

    # If the multiple question is invalid, returns an error message
    else
      render :json => {:errors => ["Invalid parameters"]}, status: 400
    end
  end

end
