class MaterialTypesController < ApplicationController
  # Validates user.
  before_action :course_admin

  # Executes this action before the specified methods
  before_action :set_material_type, only: [:show, :update, :destroy]

  # get /material_types
  def index
    @material_types = MaterialType.all
    render :json => @material_types, only: [:id, :material_type_name, :image_file_name], methods: :image_url
  end

  # get /material_types/:id
  def show
    render :json => @material_type, only: [:material_type_name, :image_file_name], methods: :image_url
  end

  # post /material_types
  def create
    # Gets the type of the file.
    @content_type = Mime::Type.lookup_by_extension(File.extname(params[:image_name])[1..-1]).to_s
    # Creates a new paperclip file, decoding the params.
    @image = Paperclip.io_adapters.for("data:#{@content_type};base64,#{params[:image_encoded]}")
    # Set the file's name.
    @image.original_filename = params[:image_name]

    # Creates the new material type.
    @material_type = MaterialType.new(material_type_name: params[:material_type_name], image: @image)

    # If the file name is valid.
    if(@material_type.image_file_name != "data")

      if(@material_type.save)
        render :json => @material_type, only: :id
      else
        render :json => {:errors =>@material_type.errors}, status: 400
      end

    # If file name wasn't provided.
    else
      render :json => {:errors =>["No filename provided!"]}, status: 400
    end
  end

  # put /material_types/:id
  # This just updates the image.
  def update
    # Gets the type of the file.
    @content_type = Mime::Type.lookup_by_extension(File.extname(params[:image_name])[1..-1]).to_s
    # Creates a new paperclip file, decoding the params.
    @image = Paperclip.io_adapters.for("data:#{@content_type};base64,#{params[:image_encoded]}")
    # Set the file's name.
    @image.original_filename = params[:image_name]

    if @material_type.update(image: @image)
      render :json => {:errors => :null}, status: 200
    else
      render :json => {:errors => @material_type.errors}, status: 400
    end
  end

  # delete /material_types/:id
  def destroy
    @material_type.destroy
    render :json => {:errors => :null}, status: 200
  end

  # Defining before_action
  def set_material_type
    @material_type = MaterialType.find(params[:id])
  end

  # Declaring private methods.
  private :set_material_type
end
