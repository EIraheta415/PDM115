class ModulesController < ApplicationController
  # Validates user.
  before_action :course_admin, except: :show
  before_action :authenticate_request, only: :show  # Students or course_admin

  # Executes this action before the specified methods
  before_action :set_module, only: [:show, :update, :destroy, :change_active]

  # get /modules/:id
  # get /modules/:id?include=materials
  def show
    # include materials?
    @inc_materials = params[:include] == "materials"

    if(@inc_materials)
      render :json => @course_module,
        include:
        [
          {
            evaluation: { only: :id}
          },
          {
            study_material: {
              except: [:material_content_type, :material_updated_at, :course_module_id],
              include: {
                material_type: {except: [:image_content_type, :image_updated_at, :image_file_size], methods: :image_url },
              },
              methods: :material_file_url
            }
          }
        ],
        except: :id
    else
      render :json => @course_module, include: {evaluation: {only: :id}}, except: :id
    end
  end

  # post /courses/:course_id/modules
  def create
    @course_module = CourseModule.new(module_create_params)
    @course_module.course_id = params[:course_id]

    if @course_module.save
      # If the course_module row is stored correctly, creates a row in the table "evaluations".
      @evaluation = Evaluation.new(course_module: @course_module)
      @evaluation.save

      # Returns the new rows id.
      render :json => {:id => @course_module.id, :evaluation_id => @evaluation.id }
    else
      render :json => {:errors => @course_module.errors}, status: 400
    end
  end

  # put /modules/:id
  def update
    if @course_module.update(module_update_params)
      render :json => {:errors => :null}
    else
      render :json => {:errors => @course_module.errors}, status: 400
    end
  end

  # put /modules/change_active/:id
  # Activate / deactivate the given module.
  def change_active
    @active = @course_module[:module_active]
    @course_module.update(module_active: !@active)
    render :json => {:errors => :null}
  end

  # delete /courses/:id
  def destroy
    @course_module.destroy
    render :json => {:errors => :null}
  end

  # Defining before_action
  def set_module
    @course_module = CourseModule.find(params[:id])
  end

  # Creation params
  def module_create_params
    params.require(:module).permit(:course_id, :module_name, :module_objective, :module_level)
  end

  # Updation params
  def module_update_params
    params.require(:module).permit(:id, :module_name, :module_objective, :module_level)
  end

  # Declaring private methods.
  private :set_module, :module_create_params, :module_update_params
end
