class QuestionsController < ApplicationController
  # Validates user.
  before_action :course_admin

  # Executes this action before the specified methods
  before_action :set_question, only: [:update, :destroy]

  # put /questions/:id
  # Modifies the statement on the given question.
  def update
    if(params[:statement])
      @question.update(question_statement: params[:statement])
      render :json => {:errors => :null}
    else
      render :json => {:errors => ["No statement provided!"]}, status: 400
    end
  end

  # delete /questions/:id
  # Deletes the row on the table "questions" and the asociated rows in the table "answers"
  # Look at the model question.rb for more details.
  # If a link question is left without questions, is deleted.
  def destroy
    @question.destroy
    render :json => {:errors => :null}
  end

  def set_question
    @question = Question.find(params[:id])
  end

  # Declaring private methods.
  private :set_question
end
