class MaterialsController < ApplicationController
  # Validates user.
  before_action :course_admin

  # Executes this action before the specified methods
  before_action :set_material, only: [:destroy, :update]

  # post /modules/:module_id/materials
  def create
    # Gets the type of the file.
    @content_type = Mime::Type.lookup_by_extension(File.extname(params[:file_name])[1..-1]).to_s
    # Creates a new paperclip file, decoding the params.
    @material_file = Paperclip.io_adapters.for("data:#{@content_type};base64,#{params[:file_encoded]}")
    # Set the file's name.
    @material_file.original_filename = params[:file_name]

    @study_material = StudyMaterial.new()
    @study_material.study_material_name = params[:study_material_name]
    @study_material.course_module_id = params[:course_module_id]
    @study_material.material_type_id = params[:material_type_id]
    @study_material.material = @material_file

    # If the file name is valid.
    if(@study_material.material_file_name != "data")

      if @study_material.save
        render :json => @study_material, only: :id
      else
        render :json => {:errors => @study_material.errors}, status: 400
      end

    # If file name wasn't provided.
    else
      render :json => {:errors =>["No filename provided!"]}, status: 400
    end
  end

  # put /materials/:id
  # Just modifies the name of the material.
  def update
    if @study_material.update(material_update_params)
      render :json => {:errors => :null}
    else
      render :json => {:errors => @study_material.errors}, status: 400
    end
  end

  # delete /materials/:id
  def destroy
    @study_material.destroy
    render :json => {:errors => :null}
  end

  # Defining before_action
  def set_material
    @study_material = StudyMaterial.find(params[:id])
  end

  # Updation params
  def material_update_params
    params.require(:material).permit(:id, :study_material_name)
  end

  # Declaring private methods.
  private :set_material, :material_update_params
end
