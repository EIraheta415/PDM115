class LinkQuestionsController < ApplicationController
  # Validates user.
  before_action :course_admin

  before_action :set_evaluation

  # post /evaluations/:evaluation_id/questions/link
  # Creates a new link question
  def create
    # Creates a link question object
    @link_question = LinkQuestion.new(params[:evaluation_id], params[:questions])

    # If the question is valid (has multiple questions with a correct answer and any number of answers)
    if (@link_question.valide?)
      @evaluation = Evaluation.find(@link_question.evaluation_id)
      @q = @evaluation.evaluation_q_link_questions

      @link_question.questions.each do |question_obj|
        @question = Question.new(evaluation_id: @link_question.evaluation_id,
                      question_statement: question_obj[:statement], question_type: "link", link_question_correlative: @q)
        @question.save

        # Stores a row in the table "answers" for the correct answer.
        @answer_correct = Answer.new(question_id: @question.id, answer_statement: question_obj[:answer_correct],
                            correct_answer: true)
        @answer_correct.save

        # Stores in the table "answers" for the incorrect answers.
        question_obj[:answers_incorrects].each do |answer_inc|
          @answer_incorrect = Answer.new(question_id: @question.id, answer_statement: answer_inc, correct_answer: false)
          @answer_incorrect.save
        end
      end

      @evaluation.update(evaluation_q_link_questions: @q + 1)

      # Returns the id of the new "question" row
      render :json => {:correlative => @q}

    # If the simple question is invalid, returns an error message
    else
      render :json => {:errors => ["Invalid parameters"]}, status: 400
    end
  end # end of def create

  # post /evaluations/:evaluation_id/questions/link/:correlative
  # Adds a new question to a link question.
  def update
    # Gets the current link questions of the evaluation.
    @link_questions = @evaluation.link(true)

    # Get the link question with the correlative param.
    @correlative = params[:correlative].to_i
    @link_question = @link_questions.select{ |l| l.correlative == @correlative}[0]

    # If the link question exists.
    if(@link_question)
      # Gets the param question.
      @q = params[:question]

      # If param exists
      if(@q)

        # If param is valid
        if( @q[:statement] && @q[:answer_correct] && @q[:answers_incorrects] && @q[:answers_incorrects].kind_of?(Array) )

          # Creates a new question row.
          @question = Question.new(evaluation_id: @evaluation.id, question_statement: @q[:statement], question_type: "link",
                        link_question_correlative: @correlative)
          @question.save

          # Stores a row in the table "answers" for the correct answer.
          @answer_correct = Answer.new(question_id: @question.id, answer_statement: @q[:answer_correct],
                              correct_answer: true)
          @answer_correct.save

          # Stores in the table "answers" for the incorrect answers.
          @q[:answers_incorrects].each do |answer_inc|
            @answer_incorrect = Answer.new(question_id: @question.id, answer_statement: answer_inc, correct_answer: false)
            @answer_incorrect.save
          end

          render :json => {"id" => @question.id}

        # If param is invalid.
        else
          render :json => {"errors" => ["Question not valid!"]}, status: 400
        end

      # If param is missing.
      else
        render :json => {"errors" => ["Param missing!"]}, status: 400
      end

    # If link question doesn't exists
    else
      render :json => {:errors => ["Correlative not found!"]}, status: 404
    end
  end # end of def update

  # delete /evaluations/:evaluation_id/questions/link/:correlative
  # Deletes a link question
  def destroy
    # Gets all the question rows.
    @questions_linked = Question.where(evaluation_id: @evaluation.id, link_question_correlative: params[:correlative].to_i)

    # If none questions were recovered.
    if(@questions_linked.empty?)
      render :json => {:errors => ["Correlative not found!"]}, status: 404

    # If at least one question were recovered.
    else
      # Destroy each question recovered.
      @questions_linked.each do |question|
        question.destroy
      end
      render :json => {:errors => :null}, status: 200
    end
  end # end onf def destroy.

  def set_evaluation
    # Checks if evaluation exists.
    @evaluation = Evaluation.find(params[:evaluation_id])
  end

  private :set_evaluation
end
