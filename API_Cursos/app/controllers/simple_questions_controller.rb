class SimpleQuestionsController < ApplicationController
  # Validates user.
  before_action :course_admin

  # post /evaluations/:evaluation_id/questions/simple
  # Creates a new simple question.
  def create
    # Checks if evaluation exists.
    @evaluation = Evaluation.find(params[:evaluation_id])

    # Creates the simple question object.
    @simple_question = SimpleQuestion.new(params[:evaluation_id], params[:statement],
                          params[:answer_correct], params[:answers_incorrects])

    # If the question is valid (has a statement, a correct answer and three incorrect answers)
    if(@simple_question.valide?)
      # Stores a row in the table "questions", with type 'simple'
      @question = Question.new(evaluation_id: @simple_question.evaluation_id,
                    question_statement: @simple_question.statement, question_type: "simple")
      @question.save

      # Stores a row in the table "answers" for the correct answer.
      @answer_correct = Answer.new(question_id: @question.id, answer_statement: @simple_question.answer_correct,
                          correct_answer: true)
      @answer_correct.save

      # Stores three rows in the table "answers" for the incorrect answers.
      @simple_question.answers_incorrects.each do |answer_inc|
        @answer_incorrect = Answer.new(question_id: @question.id, answer_statement: answer_inc, correct_answer: false)
        @answer_incorrect.save
      end

      # Returns the id of the new "question" row
      render :json => {:id => @question.id}

    # If the simple question is invalid, returns an error message
    else
      render :json => {:errors => ["Invalid parameters"]}, status: 400
    end
  end

end
