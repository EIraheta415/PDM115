class AnswersController < ApplicationController
  # Validates user.
  before_action :course_admin

  # post /questions/:question_id/answers
  def create
    @question = Question.find(params[:question_id])

    # Creating an answer depends of the type of question.
    case @question.question_type

    # For simple questions, creation is denied.
    when "simple"
      render :json => {:errors => ["Can't create new answers on a simple question!"]}, status: 400
    # end of when = "simple"

    # For multiple questions, it's allowed any type of new answer (correct or incorrect)
    when "multiple"
      if(params[:statement] && !params[:correct].nil?)
        @new_answer = Answer.new(question_id: @question.id, answer_statement: params[:statement], correct_answer: params[:correct])
        @new_answer.save
        render :json => @new_answer, only: :id
      else
        render :json => {:errors => ["Params missing! Check you send 'statement' and 'correct'"]}, status: 400
      end
    # end of when = "multiple"

    # For link questions, it's allowed only incorrect answers.
    when "link"
      if(params[:statement] && !params[:correct])
        @new_answer = Answer.new(question_id: @question.id, answer_statement: params[:statement], correct_answer: false)
        @new_answer.save
        render :json => @new_answer, only: :id
      else
        if(params[:correct])
          render :json => {:errors => ["Don't specify the type of answer, it's always incorrect!"]}, status: 400
        else
          render :json => {:errors => ["No statement provided!"]}, status: 400
        end
      end
    # end of when = "link"

    end # end of case
  end


  # put /answers/:id
  # Modifies the statement on the given answer.
  def update
    if(params[:statement])
      @answer = Answer.find(params[:id])
      @answer.update(answer_statement: params[:statement])
      render :json => {:errors => :null}, status: 200
    else
      render :json => {:errors => ["No statement provided!"]}, status: 400
    end
  end

  # delete /answers/:id
  def destroy
    @answer = Answer.find(params[:id])
    @question = Question.find(@answer.question_id)

    # Deleting an answer depends of the type of question.
    case @question.question_type

    # For simple questions, deletion is denied
    when "simple"
      render :json => {:errors => ["Can't delete answers on a simple question!"]}, status: 400
    # end of when = "simple"

    # For multiple questions, it's allowed any deletion as long as it has at least a correct answer and an incorrect answer.
    when "multiple"
      if(@answer.correct_answer)
        if(@question.correct_answers_count > 1)
          @answer.destroy
          render :json => {:errors => :null}, status: 200
        else
          render :json => {:errors => ["Can't destroy this answer, it's the only correct answer left!"]}, status: 400
        end
      else
        if(@question.incorrect_answers_count > 1)
          @answer.destroy
          render :json => {:errors => :null}, status: 200
        else
          render :json => {:errors => ["Can't destroy this answer, it's the only incorrect answer left!"]}, status: 400
        end
      end
    # end of when = "multiple"

    # For link questions, it's allowed only incorrect answers.
    when "link"
      if(!@answer.correct_answer)
        @answer.destroy
        render :json => {:errors => :null}, status: 200
      else
          render :json => {:errors => ["Can't delete the correct answer on a link question"]}, status: 400
      end
    # end of when = "link"

    end # end of case
  end # end of def destroy
end
