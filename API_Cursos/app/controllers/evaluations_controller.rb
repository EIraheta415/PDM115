class EvaluationsController < ApplicationController
  # Validates user.
  before_action :course_admin

  # put /evaluations/:id
  # Only for updating the time on the given evaluation.
  def update
    @evaluation = Evaluation.find(params[:id])

    if @evaluation.update(evaluation_update_params)
      render :json => {:errors => :null}
    else
      render :json => {:errors => @evaluation.errors}
    end
  end

  # get /evaluations/:id
  # Response the evaluation object: the evaluation time and que questions group by type.
  def show
    @evaluation = Evaluation.find(params[:id])

    render :json => {
      :evaluation_time => @evaluation.evaluation_time,
      # The questions, group by type (see evaluation.rb for more details)
      :simple => @evaluation.simple(true),
      :multiple => @evaluation.multiple(true),
      :link => @evaluation.link(true)
    }
  end

  # Updation params.
  def evaluation_update_params
    params.require(:evaluation).permit(:id, :evaluation_time)
  end

  # Declaring private methods.
  private :evaluation_update_params
end
