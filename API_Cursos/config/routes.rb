Rails.application.routes.draw do
  # Unnecesary root route
  root 'home#index'

  # Table "courses"
  resources :courses

  # Table "course_modules"
  post    "/courses/:course_id/modules" => 'modules#create'
  get     "/modules/:id" => 'modules#show'
  put     "/modules/:id" => 'modules#update'
  put     "/modules/change_active/:id" => 'modules#change_active'
  delete  "/modules/:id" => 'modules#destroy'

  # Table "evaluations"
  put     "/evaluations/:id" => 'evaluations#update'
  get     "/evaluations/:id" => 'evaluations#show'

  # Table "study_materials"
  post    "/modules/:course_module_id/materials" => 'materials#create'
  put     "/materials/:id" => 'materials#update'
  delete  "/materials/:id" => 'materials#destroy'

  # Table "material_types"
  resources :material_types

  # Creation of questions
  post    "/evaluations/:evaluation_id/questions/simple" => 'simple_questions#create'
  post    "/evaluations/:evaluation_id/questions/multiple" => 'multiple_questions#create'
  post    "/evaluations/:evaluation_id/questions/link" => 'link_questions#create'
  post    "/evaluations/:evaluation_id/questions/link/:correlative" => 'link_questions#update'

  # Table "questions"
  put     "/questions/:id" => 'questions#update'
  delete  "/questions/:id" => 'questions#destroy'
  delete  "/evaluations/:evaluation_id/questions/link/:correlative" => 'link_questions#destroy'

  # Table "answers"
  post    "/questions/:question_id/answers" => 'answers#create'
  put     "/answers/:id" => 'answers#update'
  delete  "/answers/:id" => 'answers#destroy'
end
