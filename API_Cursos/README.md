# Service specifications

## Website
[https://pseesapicursos.herokuapp.com](https://pseesapicursos.herokuapp.com)

## Gems includes
* 'rails'
* 'pg'
* 'paperclip'
* 'aws-sdk'
* 'typhoeus'

## This service manage the entities:

1. [Courses](#1-courses)
* [Modules of the course](#2-modules-of-the-course)
* [Evaluation of the module](#3-evaluation-of-the-module)
* [Questions of the evaluations](#4-questions-of-the-evaluations)
  * Simple questions
  * Multiple questions
  * Link questions
* [Answers of the question](#5-answers-of-the-questions)
* [Study materials of the module](#6-study-materials-of-the-module)
* [Types of study materials](#7-types-of-study-materials)

## Routes dictionary

#### Notes  
The json formats used for indicate the "Recieves" and "Responses" of the routes include an example:  

"?"   => string (ex: "name")  
?_??  => int    (ex: evaluation_id)   
?.??  => real   (ex: product.price)  
bool  => true or false  
image => an image  
file  => a file (PDF, Word or Excel)

### **1. Courses**

* **post** ~ */courses*  
Creates a new course, returning the new id.

Receives:
```
{"course_name": "Name of the new course", "course_objective": "Objective of the new course"}   

or status 400: Bad request!
```
Responses:
```
{"id": new_course_id}  

or status 400: Params error!  
```

* **get** ~ */courses*  
Returns all the courses.

Responses:
```
[
  {"id": course_1_id, "course_name": "Name of the course 1", "course_objective": "Objective of the course 1"},
  {"id": course_2_id, "course_name": "Name of the course 2", "course_objective": "Objective of the course 2"},
  ...
]   
```

* **get** ~ */courses/:id*  
Returns a course.

Responses:
```
{"course_name": "Name of the course", "course_objective": "Objective of the course"}   

or status 404: Not found!
```

* **get** ~ */courses/:id?include=modules*  
Returns a course with its modules

Responses:
```
{
  "course_name": "Name of the course",
  "course_objective": "Objective of the course",
  "modules":
  [
    {"id": module_1_id, "module_name": "Name of the module 1", "module_level": "Level of the module 1", "module_active": bool, "module_objective": "Objective of the module 1"},
    {"id": module_2_id, "module_name": "Name of the module 2", "module_level": "Level of the module 2", "module_active": bool, "module_objective": "Objective of the module 2"},
    ...
  ]
}   

or status 404: Not found!
```

* **put** ~ */courses/:id*  
Updates a course.

Receives:
```
{"course_name": "New name", "course_objective": "New objective"}
or
{"course_name": "New name"}   
or
{"course_objective": "New objective"}
```
Responses:
```
Status 200: (Y)
Status 400: Params error!
Status 404: Not found!   
```

* **delete** ~ */courses/:id*  
Deletes a course.

Responses:
```
Status 200: (Y)
Status 404: Not found   
```

### **2. Modules of the course**
* **post** ~ */courses/:course_id/modules*  
Creates a new module, returning the new id. Also, creates the evaluation asociated with this module, returning its id too.

Receives:
```
{"module_name": "Name of the new module", "module_level": Level_of_the_new_module, "module_objective": "Objective of the new module"}

```
Responses:
```
{"id": new_module_id, "evaluation_id": evaluation_of_the_module_id}

or status 400: bad request!
```

* **get** ~ */modules/:id*  
Gets a module

Responses:
```
{
  "course_id": course_id,
  "module_name": "Name of the module",
  "module_level": level_of_the_module,
  "module_active": bool,
  "module_objective": "Objective of the module",
  "evaluation":
  {
    "id":  evaluation_id
  }
}

or status 404: not found!
```

* **get** ~ */modules/:id?include=materials*  
Gets a module, with the asociated study materials.

Responses:
```
{
  "course_id": course_id,
  "module_name": "Name of the module",
  "module_level": level_of_the_module,
  "module_active": bool,
  "module_objective": "Objective of module",
  "evaluation": {
    "id": evaluation_id
  },
  "study_material":
  [
    {
      "study_material_name": "Name of the material 1",
      "material_file_name": "Name of the file of material 1",
      "material_file_size": "Size of the file of material 1",
      "material_file_url": "URL of the file of material 1",
      "material_type":
      {
        "material_type_name": "Name of the type of material",
        "image_file_name": "Name of the image file",
        "image_url": "URL of the image"
      }
    },
    {
      "study_material_name": "Name of the material 2",
      "material_file_name": "Name of the file of material 2",
      "material_file_size": "Size of the file of material 2",
      "material_file_url": "URL of the file of material 2",
      "material_type":
      {
        "material_type_name": "Name of the type of material",
        "image_file_name": "Name of the image file"
        "image_url": "URL of the image"
      }
    },
    ...
  ]
}
```

* **put** ~ */modules/:id*  
Updates a module.

Receives:
```
{"module_name": "New name", "module_level": new_level, "module_objective": "New objective"}

or any posible combination of the parameters (all are optional)
```
Responses:
```
Status 200: (Y)
Status 400: Params error!
Status 404: Not found!
```

* **put** ~ */modules/change_active/:id*  
Changes the "active" attibute of a module (logic delete)

Responses:
```
Status 200: (Y)
Status 404: Not found!
```

* **delete** ~ */modules/:id*  
Deletes a modules.

Responses:
```
Status 200: (Y)
Status 404: Not found  
```

### **3. Evaluation of the module**
* **put** ~ */evaluations/:id*  
Updates an evaluation (only the evaluation_time)

Receives:
```
{"evaluation_time": new_time}
```
Responses:
```
Status 200: (Y)
Status 400: Params error!
Status 404: Not found!
```

* **get** ~ */evaluations/:id*  
Gets an evaluation, with the questions asociated. Each question and answer includes its id for edition.  

Responses:
```
{
  "evaluation_time": time_of_evaluation,
  "simple": [
    {
      "question": {"id": simple_question_id_1 (sq1), "statement": "Statement of sq1" },
      "correct_answer": {"id": c_ans_1_id, "statement": "Statement of correct anwser of sq1" },
      "incorrect_answers": [
        { "id": in_ans_1_1_id, "statement": "Statement of incorrect answer 1 of sq1" },
        { "id": in_ans_1_2_id, "statement": "Statement of incorrect answer 2 of sq1" },
        ...
      ]
    },
    {
      "question": {"id": simple_question_id_2 (sq2), "statement": "Statement of sq2" },
      "correct_answer": {"id": c_ans_2_id, "statement": "Statement of correct anwser of sq2" },
      "incorrect_answers": [
        { "id": in_ans_2_1_id, "statement": "Statement of incorrect answer 1 of sq2" },
        { "id": in_ans_2_2_id, "statement": "Statement of incorrect answer 2 of sq2" },
        ...
      ]
    },
    ...
  ],
  "multiple": [
    {
      "question": { "id": multiple_question_1_id (mq1), "statement": "Statement of mq1" },
      "correct_answers": [
        { "id": c_ans_1_1_id, "statement": "Statement of correct answer 1 of mq1" },
        { "id": c_ans_1_2_id, "statement": "Statement of correct answer 2 of mq1" },
        ...
      ],
      "incorrect_answers": [
        { "id": in_ans_1_1_id, "statement": "Statement of incorrect answer 1 of mq1" },
        { "id": in_ans_1_2_id, "statement": "Statement of incorrect answer 2 of mq1" },
        ...
      ]
    },
    {
      "question": { "id": multiple_question_2_id (mq2), "statement": "Statement of mq2" },
      "correct_answers": [
        { "id": c_ans_2_1_id, "statement": "Statement of correct answer 1 of mq2" },
        { "id": c_ans_2_2_id, "statement": "Statement of correct answer 2 of mq2" },
        ...
      ],
      "incorrect_answers": [
        { "id": in_ans_2_1_id, "statement": "Statement of incorrect answer 1 of mq2" },
        { "id": in_ans_2_2_id, "statement": "Statement of incorrect answer 2 of mq2" },
        ...
      ]
    },
    ...
  ],
  "link": [
    {
      "correlative": link_correlative_1,
      "questions": [
        { "question": { "id": question_1_1_id (lq1-1), "statement": "Statement of question 1 of link 1" },
          "correct_answer": { "id": c_ans_1_1_id, "statement": "Statement of correct answer of lq1-1" },
          "incorrect_answers": [
            { "id": in_ans_1_1_1, "statement": "Statement of incorrect answer 1 of lq1-1" },
            { "id": in_ans_1_1_2, "statement": "Statement of incorrect answer 2 of lq1-1" },
            ...
          ]
        },
        { "question": { "id": question_1_2_id (lq1-2), "statement": "Statement of question 2 of link 1" },
          "correct_answer": { "id": c_ans_1_2_id, "statement": "Statement of correct answer of lq1-2" },
          "incorrect_answers": [
            { "id": in_ans_1_2_1, "statement": "Statement of incorrect answer 1 of lq1-2" },
            { "id": in_ans_1_2_2, "statement": "Statement of incorrect answer 2 of lq1-2" },
            ...
          ]
        },
        ...
      ]
    },
    {
      "correlative": link_correlative_2,
      "questions": [
        { "question": { "id": question_2_1_id (lq2-1), "statement": "Statement of question 1 of link 2" },
          "correct_answer": { "id": c_ans_2_1_id, "statement": "Statement of correct answer of lq2-1" },
          "incorrect_answers": [
            { "id": in_ans_2_1_1, "statement": "Statement of incorrect answer 1 of lq2-1" },
            { "id": in_ans_2_1_2, "statement": "Statement of incorrect answer 2 of lq2-1" },
            ...
          ]
        },
        { "question": { "id": question_2_2_id (lq2-2), "statement": "Statement of question 2 of link 2" },
          "correct_answer": { "id": c_ans_2_2_id, "statement": "Statement of correct answer of lq2-2" },
          "incorrect_answers": [
            { "id": in_ans_2_2_1, "statement": "Statement of incorrect answer 1 of lq2-2" },
            { "id": in_ans_2_2_2, "statement": "Statement of incorrect answer 2 of lq2-2" },
            ...
          ]
        },
        ...
      ]
    },
    ...
  ]
}
```

### **4. Questions of the evaluations**
* **post** ~ */evaluations/:evaluation_id/questions/simple*  
Creates a new simple question for the specified evaluation, returning the new id.

Receives:
```
{
  "statement": "Question statement",
  "answer_correct": "Correct answer statement",
  "answers_incorrects": ["Incorrect answer 1", "Incorrect answer 2", "Incorrect answer 3"]
}
```
Responses:
```
{"id": new_question_id}

or status 400: bad request!
or status 404: evaluation not found!
```

* **post** ~ */evaluations/:evaluation_id/questions/multiple*  
Creates a new multiple question for the specified evaluation, returning the new id.

Receives:
```
{
  "statement": "Question statement",
  "answers_corrects": ["Correct answer 1", "Correct answer 2", ... ],
  "answers_incorrects":["Incorrect answer 1", "Incorrect answer 2", ... ]
}
```
Responses:
```
{"id": new_question_id}

or status 400: bad request!
or status 404: evaluation not found!
```

* **post** ~ */evaluations/:evaluation_id/questions/link*  
Creates a new link question for the specified evaluation, returning the new correlative.

Receives:
```
{
	"questions":[
		{
      "statement":"Question 1 (q1)",
      "answer_correct": "Correct answer of q1",
      "answers_incorrects":["Incorrect answer 1 of q1", "Incorrect answer 2 of q1", ... ]
    },
    {
      "statement":"Question 2 (q2)",
      "answer_correct": "Correct answer of q2",
      "answers_incorrects":["Incorrect answer 1 of q2", "Incorrect answer 2 of q2", ... ]
    },
    ...
	]
}
```
Responses:
```
{"correlative": new_question_correlative}

or status 400: bad request!
or status 404: evaluation not found!
```

* **post** ~ */evaluations/:evaluation_id/questions/link/:correlative*  
Adds a new questios to a existing link question. Don't use this route for create simple or multiple questions.  

Receives:
```
{
  "question":{
    "statement": "Question statement",
    "answer_correct": "Correct answer statement",
    "answers_incorrects": ["Incorrect answer 1", "Incorrect answer 2", ... ]
  }  
}
```
Responses:
```
{"id": new_question_id}

Status 400: Params error!
Status 404: Not found!
```

* **put** ~ */questions/:id*  
Updates the statement of a question.

Receives:
```
{"statement": "New statement"}
```
Responses:
```
Status 200: (Y)
Status 400: Params error!
Status 404: Not found!
```

* **delete** ~ */questions/:id*  
Deletes a question.

Responses:
```
Status 200: (Y)
Status 404: Not found!
Status 400: Bad request!
```

* **delete** ~ */evaluations/:evaluation_id/questions/link/:correlative*  
Deletes a link question.

Responses:
```
Status 200: (Y)
Status 404: Not found!
```

### **5. Answers of the questions**
* **post** ~ */questions/:question_id/answers*  
Creates a new answer for the specified question. Simple questions don't accept new answers, multiple questions accept any kind of new answer and link questions accept only new incorrect answers.

Receives:
```
For multiple question:
{"statement": "New answer", "correct": bool}

For link question:
{"statement": "New statement"}
```
Responses:
```
{"id": new_answer_id}

or status 404: Question not found!
or status 400: Bad request!
```

* **put** ~ */answers/:id*  
Updates the statement of an answer.

Receives:
```
{"statement": "New statement"}
```
Responses:
```
Status 200: (Y)
Status 400: Params error!
Status 404: Not found!
```

* **delete** ~ */answers/:id*  
Deletes an answer. Deletion is just for multiple questions, as long as there is at least a correc and an incorrect answer left after deleting; and for link questions if the answer is incorrect.

Responses:
```
Status 200: (Y)
Status 400: Bad request!
Status 404: Not found!
```

### **6. Study materials of the module**

* **post** ~ */modules/:module_id/materials*  
Creates a new material returning the new id.

Receives:
```
{
  "material_type_id": id_of_the_material_type,
  "study_material_name": "Name of the new study material"
  "file_name": "Name of the new file asociated with the study material",
  "file_encoded": "File encoded in base64"
}
```
Responses:
```
{"id": new_material_id}

or status 404: Course not found!
or status 400: Params error!
```  

* **put** ~ */materials/:id*  
Update the name of a study material

Receives:
```
{"study_material_name": "New name for study material"}
```
Responses:
```
Status 200: (Y)
Status 400: Params error!
Status 404: Not found!
```

* **delete** ~ */materials/:id*  
Deletes a material.

Responses:
```
Status 200: (Y)
or status 404: Not found!
```

### **7. Types of study materials**
* **post** ~ */material_types*  
Creates a new material type.

Receives:
```
{
  "material_type_name": "Name of the new material type",
  "image_name": "Name of the new image added",
  "image_encoded": "Image encoded in base64"
}
```
Responses:
```
{"id": new_material_type_id}

or status 400: Params error!
```

* **get** ~ */material_types*  
Gets all material types

Responses:
```
[
  {"id": material_type_id_1, "material_type_name": "Name of material type 1", "image_file_name": "Name of the image file", "image_url": "URL of the image"},
  {"id": material_type_id_2, "material_type_name": "Name of material type 2", "image_file_name": "Name of the image file", "image_url": "URL of the image"},
  ...
]
```

* **get** ~ */material_types/:id*  
Get a material type.

Responses:
```
{"material_type_name": "Name of the material type", "image_file_name": "Name of the image file", "image_url": "URL of the image"}

or status 404: Not found!
```

* **put** ~ */material_types/:id*  
Updates a material type. Just for update the image.

Receives:
```
{
  "image_name": "Name of the new image added",
  "image_encoded": "Image encoded in base64"
}
```
Responses:
```
Status 200: (Y)
Status 400: Params error!
Status 404: Not found!
```

* **delete** ~ */material_types/:id*  
Deletes a material type.

Responses:
```
Status 200: (Y)
or status 404: not found!
```

### **Unnecesary root route**

* **get** ~ */*  
Does nothing useful.

Response:

```
{"message": "Welcome message"}   
```
