class OrderDetail < ActiveResource::Base
  self.site = "https://pseesapipedidos.herokuapp.com" #coneccion a API de Pedidos
	

	def self.getSoldProducts(id)  #Devuelve El numero de Productos vendidos por pedido a partir de id
    
    
    if @details=OrderDetail.find(:all, :from => "/order_details/product_delivered/"+id.to_s)

      @counter = @details.count
      @sum = 0

      @details.each_with_index do |detail,i|
        @amounts = @details[i].quantity.to_f #Se obtiene las cantidades de productos por pedido
        @sum = @sum + @amounts
      end

      if @counter > 0
        @result = @sum/@counter
      else 
        @result= -1
      end
      return @result.round.to_i
    else
      return -1
    end
    
  end

  def self.getScores(id) #Devuelve y calcula el puntaje promedio en cada pedido
    
    
    if @details = OrderDetail.find(:all, :from => "/order_details/product_delivered/"+id.to_s)
      @counter = @details.count
      @perc = 0

      @details.each_with_index do |detail,i|
        @percentage = @details[i].score.to_f #Se obtienen los puntajes de los pedidos
        @perc = @perc + @percentage
      end

      if @counter > 0
        @result = @perc/@counter
      else 
        @result = -1
      end
      return @result.round(2)
    else
      return -1
    end
    
  end

end



      
      
      
         
          
          
        

      
      
      