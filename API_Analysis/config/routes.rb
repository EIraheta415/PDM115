Rails.application.routes.draw do
  resources :order_details
  get 'details/products/:id', to: 'order_details#getSold'
  


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
