class CourseModuleSerializer < ActiveModel::Serializer
  attributes :id, :module_name, :module_level, :module_active, :module_objective
  #has_one :course
end
