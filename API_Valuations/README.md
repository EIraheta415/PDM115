# Specifications

## URL
[https://pseesapivaluations.herokuapp.com](https://pseesapivaluations.herokuapp.com)

## Gems includes

* 'rails'
* 'pg'
* 'typhoeus'
* 'jbuilder'
* 'json'

### Notes
The jbuilder get responses has to end with a .json extension in the url

## Services

1. [Order_Details](#1-order-details)
*  [Products](#2-products)

## Routes dictionary

### **1. Order_Details**

* **get** ~ */values/:product_id.json*
Retrieves data list about score, comment and client of a product

Responses:
```
{"oder":"The order ID","first_name":"Name of client","last_name":"Last name of the client","score":"Valuation of the client","commentary":"Commentary by the client"}

or status 400: Product not found
```

* **patch/put** ~ */score_update*
Update the value of the score in the order detail and the product register

Receives:

```
{"id":"Id of the order","product_id":"Id of the product","score":"Valuation of the client"}
```

Responses:

```
status 200: Score updated
or status 400: Unset score
or status 400: Product not found
```

* **patch/put** ~ */commentary_update*
Update the commentary in the order detail of the order made by the client

Receives:
```
{"id":"Id of the order" , "product_id":"Id of the product" ,"commentary":"Comment sent by the client"}
```

Responses:

```
status 200: Commentary updated
od status 400: Product not found
```

### **2. Products**

* **get** ~ */valuations*
Retrieves the data list of the product in format id - name - score

Responses:
```
[{"id":"Product id", "product_name":"Product name", "score":"General score of the product"}, {...}, ...]
```

* **get** ~ */valuations/:id*
Retrieves the data of a single product in format id - name - score

Responses:
```
{"id":"Product id", "product_name":"Product name", "score":"General score of the product"}
```
