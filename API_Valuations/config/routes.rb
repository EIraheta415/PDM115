Rails.application.routes.draw do
  #Custom routes creation
  get "values/:product_id", to: "order_details#score_commentary"
  get "valuations", to: "products#index_valuations"
  get "valuations/:id", to: "products#show_valuation"
  patch "valuations/score_update", to: "order_details#update_score"
  put "valuations/score_update", to: "order_details#update_score"
  patch "valuations/commentary_update", to: "order_details#update_commentary"
  put "valuations/commentary_update", to: "order_details#update_commentary"
end
