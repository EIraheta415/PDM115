class ProductsController < ApplicationController
  before_action :set_product, only: [:show_valuation]

  # GET /valuations Header: Authorization:token
  def index_valuations
    @products = Product.all
    render json: @products, each_serializer: ProductSerializer::Valuations
  end

  # GET /valuations/:id Header: Authorization:token
  def show_valuation
    render json: @product, serializer: ProductSerializer::Valuations
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end
end
