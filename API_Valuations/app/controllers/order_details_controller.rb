class OrderDetailsController < ApplicationController
  before_action :set_order_detail, except: [:score_commentary]

  def score_commentary
    begin
      if params[:product_id].blank? == false
        @order_detail = OrderDetail.where("product_id = ?", params[:product_id])
        #The return values are coming from views -> order_details -> score_commentary.json.jbuilder
      else
        render :json => {:errors => "Product not found"}, status: 400
      end
    rescue ActiveRecord::RecordNotFound
      #Returns error message
      render :json => {:errors => ["Product not found!"]}, status: 400
    end
  end

  #Protocol PATCH/PUT /valuations/order_id  Header: Authorization:token,Content-type:application/json | json{"product_id":id,"score":score}
  def update_score
    begin
        @order_detail = OrderDetail.find(params[:id])
        od = @order_detail
        if params[:score].blank? == false && params[:product_id].blank? == false
          product = Product.find(params[:product_id])
          cont = product.num_scores
          total = 0
          #Evaluate if the product has been valuated before
          if  cont == 0
            cont += 1
            total = BigDecimal(params[:score])
            #update to de number of valuations
            product.update_attribute(:num_scores, cont)
          else
            #if the valuation of the client has to bee updated
            total = product.score * product.num_scores
            if @order_detail.score.nil? == true
              cont += 1
              total += BigDecimal(params[:score])
              product.update_attribute(:num_scores, cont)
            else
              total = total - @order_detail.score
              total = total + BigDecimal(params[:score])
            end
          end
          n_score = total / cont
          @order_detail.update_attribute(:score, params[:score])
          product.update_attribute(:score, n_score)
          render :json => [:success => "Score updated"], status: 200
        else
            render :json => {:error => "Unsent score"}, status: 400
        end
    rescue ActiveRecord::RecordNotFound
      #Returns error message
      render :json => {:errors => ["Product not found"]}, status: 400
    end
  end

  #Protocol PATCH/PUT /valuation/order_id  Header: Authentication:token,Content-type:application/json | json{"product_id":"","commentary":""}
  def update_commentary
    begin
      product = Product.find(params[:product_id])
      @order_detail = OrderDetail.find(params[:id])
      @order_detail.update_attributes(:commentary => params[:commentary])
      #Returns success message
      render :json => {:sucess => ["Commentary updated!"]}, status: 200
    rescue ActiveRecord::RecordNotFound
      #Returns error message
      render :json => {:errors => ["Product not found!"]}, status: 400
    end
  end

  private
    def set_order_detail
      @order_detail = OrderDetail.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_detail_params
      params.require(:order_detail).permit(:order_id, :product_id, :quantity, :sale_price, :amount, :score, :commentary)
    end
end
