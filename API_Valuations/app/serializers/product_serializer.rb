
class ProductSerializer < ActiveModel::Serializer
  attributes :id, :product_name, :description, :price, :stock, :score, :new, :available, :active, :image_file_name, :image_content_type, :image_file_size, :image_updated_at, :num_scores
end

class ProductSerializer::Valuations < ActiveModel::Serializer
  attributes :id, :product_name, :score
end
