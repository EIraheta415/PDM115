
class OrderDetailSerializer < ActiveModel::Serializer
  attributes :id, :quantity, :sale_price, :amount, :score, :commentary
  has_one :order
  has_one :product
end

class OrderDetailSerializer::ScoreAndComment < ActiveModel::Serializer
  attributes :id, :score, :commentary
  has_one :product
end
