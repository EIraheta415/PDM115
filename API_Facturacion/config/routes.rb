Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :invoices, except: [:destroy,:update]
  resources :invoice_details, except: [:destroy,:update]
end
