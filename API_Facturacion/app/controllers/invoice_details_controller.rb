class InvoiceDetailsController < ApplicationController
  before_action :set_invoice_detail, only: [:show, :update, :destroy]

  # GET /invoice_details
  def index
    @invoice_details = InvoiceDetail.all

    render json: @invoice_details
  end

  # GET /invoice_details/1
  def show
    render json: @invoice_detail
  end

  # POST /invoice_details
  def self.create(order_id,invoice_id)
    #Se buscan los detalles de pedido del pedido
    order_details=OrderDetail.find(:all, :from => "/order_details/order/"+order_id.to_s)
    order_details.each do |order_detail|
      #Se buscan los productos de cada detalle de pedido
      product = Product.find(order_detail.product_id)
      @invoice_detail = InvoiceDetail.new(invoice_id: invoice_id,description:(product.product_name+" "+order_detail.quantity.to_s+" "+product.measure.abbreviation),subtotal: order_detail.amount)
      if @invoice_detail.save
        render json: @invoice_detail, status: :created, location: @invoice_detail
      else
        render json: @invoice_detail.errors, status: :unprocessable_entity
      end
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice_detail
      @invoice_detail = InvoiceDetail.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def invoice_detail_params
      params.fetch(:invoice_detail, {})
    end
end
