class InvoicesController < ApplicationController
  before_action :set_invoice, only: [:show, :update, :destroy]

  # GET /invoices
  def index
    @invoices = Invoice.all

    render json: @invoices
  end

  # GET /invoices/1
  def show
    render json: @invoice
  end

  # POST /invoices
  def create
    id_order=params[:id_order].to_i
    @order= Order.find(id_order)
    #Se crea una nueva factura añadiendo su respectivo pedido, el codigo es generado en formato timestamp UTC, la fecha de la factura es la actual y el total es total+iva
    @invoice = Invoice.new(order_id:@order.id,code: DateTime.now.to_time.to_i.to_s,date_invoice: DateTime.now, amount:(@order.amount * 0.13)+@order.amount)
    if @invoice.save
      InvoiceDetailsController.create(@order.id,@invoice.id)
      render json: @invoice, status: :created, location: @invoice
    else
      render json: @invoice.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def invoice_params
      params.fetch(:invoice, {})
    end
end
