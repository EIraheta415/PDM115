class InvoiceDetailSerializer < ActiveModel::Serializer
  attributes :id, :invoice_id, :description, :subtotal
  belongs_to :invoice
end
