class InvoiceSerializer < ActiveModel::Serializer
  attributes :id , :order_id, :code, :date_invoice, :amount
  has_many :invoice_details
end
