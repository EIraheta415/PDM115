class OrderDetail < ActiveResource::Base
  self.site = "https://pseesapipedidos.herokuapp.com/"
  belongs_to :order
end
